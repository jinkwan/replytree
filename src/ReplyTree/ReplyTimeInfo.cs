﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class ReplyTimeInfo {
    public DateTime timeStamp { get; set; }

    public ReplyTimeInfo(string ymd, string hm) {
      string[] ymdline = ymd.Split('/');
      string[] hmline = hm.Split(':');

      if(ymdline[0].Length < 4) {
        ymdline[0] = "20" + ymdline[0];
      }

      timeStamp = new DateTime(int.Parse(ymdline[0]), int.Parse(ymdline[1]), int.Parse(ymdline[2]),
        int.Parse(hmline[0]), int.Parse(hmline[1]), 0);
    }

    public ReplyTimeInfo() {
      timeStamp = new DateTime();
    }

    public static bool operator >(ReplyTimeInfo a, ReplyTimeInfo b) {
      return a.timeStamp > b.timeStamp ? true : false; ;
    }
    public static bool operator <(ReplyTimeInfo a, ReplyTimeInfo b) {      
      return a.timeStamp < b.timeStamp ? true : false;
    }
    public static bool operator ==(ReplyTimeInfo a, ReplyTimeInfo b) {
      return a.timeStamp == b.timeStamp ? true : false;
    }
    public static bool operator !=(ReplyTimeInfo a, ReplyTimeInfo b) {
      return a.timeStamp != b.timeStamp ? true : false;
    }
    public static int operator -(ReplyTimeInfo a, ReplyTimeInfo b) {
      int d, h, m;
      TimeSpan diff = a.timeStamp - b.timeStamp;
      d = diff.Days;
      h = diff.Hours;
      m = diff.Minutes;
      return (d * 1440 + h * 60 + m);
    }
  }
}
