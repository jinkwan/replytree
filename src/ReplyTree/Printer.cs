﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ReplyTree
{
  class Printer
  {
    StreamWriter sw;
    FolderBrowserDialog fd = new FolderBrowserDialog();

    public Printer() {
      fd.SelectedPath = ".\\";
    }

    public void PrintInfo(List<Article> alist, int nt, int dt) {
      if(fd.ShowDialog() == DialogResult.OK) {
        string line = "";
        sw = new StreamWriter(fd.SelectedPath + "\\article.csv", false, Encoding.UTF8);

        ///TODO : internal path length sum, external path length sum, 
        ///가장 깊은 노드 3개의 path length sum 등 추가
        line = "articleNo,#view,time,#reply,#partic,depth,degree,diffNP,#leaf,last,lifetime,tempSum,ipl,xpl";
        sw.WriteLine(line);

        foreach (Article a in alist) {
          if (a.depth > dt && a.numNodes > nt) {
            line = a.articleNo.ToString() + ",";
            line += a.view.ToString() + ",";
            line += a.writer.timeInfo.timeStamp.ToString() + ",";
            line += a.numNodes.ToString() + ",";
            line += a.participants.Count.ToString() + ",";
            line += a.depth.ToString() + ",";
            line += a.maxDegree.ToString() + ",";
            line += (a.numNodes - a.participants.Count()).ToString() + ",";
            line += a.numLeaf.ToString() + ",";
            line += a.lastReply.timeInfo.timeStamp.ToString() + ",";
            line += a.lifetime.ToString() + ",";
            line += a.tempSum.ToString() + ",";
            line += a.ipl.ToString() + ",";
            line += a.xpl.ToString();
            sw.WriteLine(line);
          }
        }
        sw.Close();
      }      
    }
    public string PrintInfo(Dictionary<string, UserInfo> ulist) {
      string line = "";

      foreach(UserInfo u in ulist.Values) {

      }

      return line;
    }


    #region quicksort functions
    public int Partition(ReplyNode[] nodes, int left, int right) {
      ReplyNode pivot = nodes[left];
      while(true) {
        while (nodes[left] < pivot) left++;
        while (nodes[right] > pivot) right++;

        if (left < right) {
          ReplyNode tmp = nodes[right];
          nodes[right] = nodes[left];
          nodes[left] = tmp;
        }
        else return right;
      }
    }

    public void Quicksort(ReplyNode[] nodes, int left, int right) {
      if(left < right) {
        int pivot = Partition(nodes, left, right);

        if (pivot > 1) Quicksort(nodes, left, pivot - 1);
        if (pivot + 1 < right) Quicksort(nodes, pivot + 1, right);
      }
    }
    #endregion
    
    public void PrintTemperature(Article a, int k, int c0, int c1, double dc) {
      int i, j, diff;
      ReplyNode prev, curr;
      double temp = a.InitialTemperature();
      string line = "";

      if (fd.ShowDialog() == DialogResult.OK) {
        sw = new StreamWriter(fd.SelectedPath + "\\" + a.articleNo.ToString() + "_temp.csv", false, Encoding.UTF8);
        
        line = "time,temp.";
        sw.WriteLine(line);

        line = a.writer.timeInfo.timeStamp.ToString() + "," + temp.ToString();
        sw.WriteLine(line);

        prev = a.writer;
        for (i = 0; i < a.replyList.Count(); i++) {
          curr = a.replyList[i];
          diff = curr.timeInfo - prev.timeInfo;

          
          for (j = 0; j < diff; j++) {
            prev.timeInfo.timeStamp = prev.timeInfo.timeStamp.AddMinutes(1);
            temp = temp * dc;
            line = prev.timeInfo.timeStamp.ToString() + "," + temp.ToString();
            sw.WriteLine(line);

            if (temp < Constants.TEMP_DEAD_THRESHOLD) break;
          }

          temp += curr.Temperature(k, c0, c1);
          line = curr.timeInfo.timeStamp.ToString() + "," + temp.ToString();
          sw.WriteLine(line);

          if (temp < Constants.TEMP_DEAD_THRESHOLD) break;
        }
        sw.Close();
      }
    }
  }
}
