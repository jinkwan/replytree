﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using yWorks.Canvas.Drawing;
using yWorks.yFiles.UI.Model;
using yWorks.yFiles.UI.Drawing;
using yWorks.yFiles.Algorithms;

using yWorks.yFiles.Layout;
using yWorks.yFiles.Layout.Circular;
using yWorks.yFiles.Layout.Tree;
using yWorks.yFiles.Layout.Hierarchic;
using yWorks.yFiles.Layout.Organic;
using yWorks.yFiles.Layout.Orthogonal;
using yWorks.yFiles.UI.Input;
using yWorks.Canvas.Geometry.Structs;
using C1.Win.C1Chart;

namespace ReplyTree
{
  public partial class Form1 : Form
  {   
    /// <summary>
    /// 전체 게시물 목록
    /// </summary>
    List<Article> articleList = new List<Article>();

    /// <summary>
    /// 유저 정보 파일을 기준으로 생성한 유저 정보 목록
    /// </summary>
    Dictionary<string, UserInfo> userList = new Dictionary<string, UserInfo>();

    /// <summary>
    /// 아티클 정보 파일을 기준으로 생성한 유저 정보 목록
    /// </summary>
    //Dictionary<string, UserInfo> actualUserList = new Dictionary<string, UserInfo>();

    /// <summary>
    /// 유저간 대댓글 연결성
    /// </summary>
    //int[,] connectivity = new int[Constants.MAX_MEMBER,Constants.MAX_MEMBER];
    Dictionary<EdgePair, int> conn = new Dictionary<EdgePair, int>(new EdgePairComparer());
    

    Color EDGE_BIDIRECTION = Color.DarkRed;
    Color EDGE_NORMAL = Color.DeepSkyBlue;

    Printer cp = new Printer();

    #region Standard Functions(Form, Initiation, Graph Setting, Graph Drawing)
    public Form1() {
      InitializeComponent();
      Initialize();
      TempChartSetting();
      GraphSetting();
    }

    private void Initialize() {
      int i, j;
      
      loadArticleBtn.Enabled = false;
      articleListCb.Enabled = false;
      userNetworkBtn.Enabled = false;

      articleFileoutBtn.Enabled = false;
      userFileoutBtn.Enabled = false;
      saveArticleTempBtn.Enabled = false;


      numericDepthThreshold.Enabled = false;
      numericEdgeThreshold.Enabled = false;
      numericRatio.Enabled = false;
      numericNodeThreshold.Enabled = false;
      numericK.Enabled = false;
      numericC0.Enabled = false;
      numericC1.Enabled = false;
      numericDecRat.Enabled = false;
      numericDeadDuration.Enabled = false;
      btnTempParamDefault.Enabled = false;
      numericFocused.Enabled = false;
      
      articleList.Clear();
      userList.Clear();   

      /*
      for (i = 0; i < Constants.MAX_MEMBER; i++) {
        for(j=0;j<Constants.MAX_MEMBER;j++) {
          connectivity[i, j] = 0;
        }
      }
      */
      conn.Clear();
      articleListCb.Items.Clear();
    }

    //화면 view를 움직일 수 있어야
    //노드는 선택해서 움직일 수 있어야
    public void GraphSetting() {
      GraphViewerInputMode gvim = new GraphViewerInputMode();
      graphControl1.InputMode = gvim;
      //GraphEditorInputMode geim = new GraphEditorInputMode();
      //graphControl1.InputMode = geim;
    }

    public void TempChartSetting() {
      ChartDataSeries cs = chartTemperature.ChartGroups.Group0.ChartData.SeriesList.AddNewSeries();
      cs.LineStyle.Thickness = (int)numericLineSize.Value;
      cs.LineStyle.Color = Color.Blue;
      cs.SymbolStyle.Size = (int)numericSymbolSize.Value;
      cs.SymbolStyle.Shape = SymbolShapeEnum.Dot;
      cs.SymbolStyle.Color = Color.Blue;

      chartTemperature.ChartArea.AxisX.Text = "LIFETIME";
      chartTemperature.ChartArea.AxisY.Text = "TEMPERATURE";
    }

    private void DrawGraph(yWorks.yFiles.UI.GraphControl gc, CanonicMultiStageLayouter l) {
      gc.MorphLayout(l, TimeSpan.FromSeconds(2), null);
    }
    private void radioCircular_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(graphControl1, new CircularLayouter());      
    }
    private void radioHierachical_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(graphControl1, new HierarchicLayouter());
    }
    private void radioOrthogonal_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(graphControl1, new OrthogonalLayouter());
    }
    private void radioTree_CheckedChanged(object sender, EventArgs e) {
      //TODO : tree 확인 루틴이 추가되어야 함
      DrawGraph(graphControl1, new TreeLayouter());
    }
    private void radioOrganic_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(graphControl1, new OrganicLayouter());
    }
    #endregion

    #region File I/O

    #region Load Buttons
    private void loadUserBtn_Click(object sender, EventArgs e) {
      LoadUsers();
      logText.Text = "Load User Info Finished";
    }
    private void loadArticleBtn_Click(object sender, EventArgs e) {
      LoadArticles();      
      SetActualUserInfo();
      //CalcUserConnectivity();
    }
    #endregion

    #region Loading Functions
    private void LoadUsers() {
      string line;
      char sp;
      FileInfo f;
      StreamReader sr;      

      OpenFileDialog openDlg = new OpenFileDialog();
      openDlg.Filter = "user info file|*.txt";
      openDlg.Multiselect = true;
      if (openDlg.ShowDialog() == DialogResult.OK) {
        Initialize();
        foreach (string filename in openDlg.FileNames) {
          f = new FileInfo(filename);

          sp = '\t';
          sr = f.OpenText();

          while ((line = sr.ReadLine()) != null) {
            string[] spstring = line.Split(sp);
            if (spstring[0][0] == '%') continue;
            else {
              userList[spstring[0]] = new UserInfo(spstring[0], spstring[1], 
                int.Parse(spstring[2]), int.Parse(spstring[3]));
            }
          }
          sr.Close();
        }
        loadArticleBtn.Enabled = true;
      }
    }
    private void LoadArticles() {
      FileInfo f;
      StreamReader sr;
      string articleLine, replyLine;

      OpenFileDialog openDlg = new OpenFileDialog();
      openDlg.Filter = "article file|*.txt";
      openDlg.Multiselect = true;
      if (openDlg.ShowDialog() == DialogResult.OK) {
        //Initialize();
        foreach (string filename in openDlg.FileNames) {
          f = new FileInfo(filename);
          sr = f.OpenText();
          while ((articleLine = sr.ReadLine()) != null) {
            replyLine = sr.ReadLine();
            AddNewArticle(articleLine, replyLine);   
          }
          sr.Close();
        }

        articleListCb.Enabled = true;
        userNetworkBtn.Enabled = true;
        articleFileoutBtn.Enabled = true;
        userFileoutBtn.Enabled = true;
        saveArticleTempBtn.Enabled = true;
      }
      logText.AppendText(Environment.NewLine + "Article Load Completed");
    }
    private void AddNewArticle(string articleLine, string replyLine) {
      int i;
      char sp = ' ';
      string itemName;
      string[] aInfo = articleLine.Split(sp); //새 아티클 생성
      string[] rInfo = replyLine.Split(sp);

      ReplyTimeInfo lastReplyTime;

      //글쓴이 depth는 writer_depth로 설정
      //참고로 시간이 null인 경우가 있음. 따라서 i+4가 null인지 확인해보고 i+5를 하던가 말던가 함
      if (rInfo[4] == "null") {
        lastReplyTime = new ReplyTimeInfo();
      }
      else {
        lastReplyTime = new ReplyTimeInfo(rInfo[4], rInfo[5]);
      }

      ReplyNode parentNode = new ReplyNode("0", null, 0, Constants.WRITER_DEPTH-1, new ReplyTimeInfo());
      ReplyNode currNode = new ReplyNode(rInfo[0], parentNode, int.Parse(rInfo[2]),parentNode.depth+1, lastReplyTime);
      Article na = new Article(int.Parse(aInfo[1]), int.Parse(aInfo[2]), currNode);

      for (i = 6; i < rInfo.Length; i++) {
        switch (rInfo[i]) {
          case "(":
            ///현재노드 -> 부모노드
            parentNode = currNode;
            break;
          case ")":
            ///현재노드의 부모노드 -> 부모노드            
            parentNode = currNode.parent.parent;
            currNode = currNode.parent;
            break;
          case "#":            
            break;
          case "\0":            
            break;
          case "":
            break;
          default:
            //새노드 => 현재노드
            currNode = new ReplyNode( rInfo[i], parentNode, int.Parse(rInfo[i + 2]), parentNode.depth + 1, 
                                      new ReplyTimeInfo(rInfo[i + 4], rInfo[i + 5]));
            
            //부모노드의 자식 리스트에 추가
            parentNode.AddChild(currNode);

            //기존에 등장했던 노드면 댓글 작성 개수만 증가시킴
            if (na.participants.ContainsKey(rInfo[i])) {
              na.participants[rInfo[i]]++;
            }
            else {
              na.participants[rInfo[i]] = 1;
            }
            i += 5;            
            break;
        }
      }

      //아티클의 댓글 정보, 트리 정보 계산, 댓글 리스트 정렬
      na.CalcReplyCountInfo();
      na.CalcTreeInfo();
      
      //일정 depth 이하로 내려가지 않을 경우 삭제된 글로 판단, 목록에 집어넣지 않음
      if (na.depth > Constants.ARTICLE_DEPTH_THRESHOLD && na.numNodes > 0) {
        na.SortReplyListByTimestamp();
        articleList.Add(na);
        itemName = na.articleNo.ToString() + " (depth:" + na.depth.ToString()
          + ", numNodes:" + na.numNodes.ToString() + ")";
        articleListCb.Items.Add(itemName);
        BuildArticleSubtrees(articleList.Count - 1);
      }
    }
    public void SetActualUserInfo() {
      foreach(Article article in articleList) {
        userList[article.writer.id].num_article_actual[article.articleNo] = article.numNodes;
        foreach(string user in article.participants.Keys) {
          userList[user].num_reply_actual[article.articleNo] = article.participants[user];
        }
      }
      logText.AppendText(Environment.NewLine + "SetUserInfo Finished");
    }    
    #endregion

    #region Save Buttons
    //리스트에서 선택된 옵션을 출력
    private void fileoutBtn_Click(object sender, EventArgs e) {

      //TODO : 나중에는 각 아티클을 읽는 과정에서 미리 온도 시리즈를 만들어두도록 변경하거나 하자
      Dictionary<int, double> temp = new Dictionary<int, double>();
      foreach (Article a in articleList) {
        temp.Clear();
        CalcTempSeries(a, ref temp);
      }

      CSVFileout(0); 
    }

    private void csvFileoutCb_SelectedIndexChanged(object sender, EventArgs e) {
      //여기선 무엇을 하지..
    }

    public void CSVFileout(int type) {      
      switch(type) {
        case 0:
          cp.PrintInfo(articleList, (int)numericNodeThreshold.Value, (int)numericDepthThreshold.Value);
          break;
        case 1:
          cp.PrintInfo(userList);
          break;
        default:
          break;
      }
    }


    //TODO : 이것도 printer로 보내도록
    /// <summary>
    /// 각 글의 피쳐를 생성하는 함수
    /// (피쳐 : 글id, 댓글수, 깊이1, 2, 3 노드수, 최대깊이, internal 노드수, 
    /// 댓글 참가자 수, 모든 에지의 다른 글에서의 등장 수 총합, 에지 종류 수)    
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="ea"></param>
    private void btnMatView_Click(object sender, EventArgs ea) {
      int i, d1,d2,d3,leaf, w, e;

      //for traversal
      ReplyNode p, c;
      Queue<ReplyNode> userQ = new Queue<ReplyNode>();

      //for fileout
      string line;
      StreamWriter sw;
      FolderBrowserDialog fd = new FolderBrowserDialog();
      fd.SelectedPath = ".\\";

      Dictionary<EdgePair, int> edgelist = new Dictionary<EdgePair, int>(new EdgePairComparer());
      EdgePair edge;
      if (fd.ShowDialog() == DialogResult.OK) {
        sw = new StreamWriter(fd.SelectedPath + "\\viewMat_" +
          Constants.WRITER_DEPTH.ToString() + ".csv", false, Encoding.UTF8);
        line = "article,#reply,#d1,#d2,#d3,depth,#internal,#part,W,#e";
        sw.WriteLine(line);
        foreach (Article a in articleList) {

          //정보 생성 과정
          //1.초기화
          d1 = d2 = d3 = leaf = w = e = 0;
          edgelist.Clear();
          //2.tree traversal - d1,2,3,leaf,tmpConn 계산
          userQ.Enqueue(a.writer);
          while (userQ.Count > 0) {
            p = userQ.Dequeue();
            //d1,2,3
            if (p.depth == 1) d1++;
            else if (p.depth == 2) d2++;
            else if (p.depth == 3) d3++;
            //leaf
            if (p.children.Count == 0 && p.depth > Constants.WRITER_DEPTH) leaf++;
            //tmpConn
            for (i = 0; i < p.children.Count; i++) {
              c = p.children[i];
              userQ.Enqueue(c);
              edge = new EdgePair(int.Parse(p.id), int.Parse(c.id));

              if (edgelist.ContainsKey(edge)) {
                edgelist[edge] += 1;
              }
              else {
                edgelist[edge] = 1;
              }
            }
          }

          foreach (EdgePair ep in edgelist.Keys) {
            e += 1;
            w += conn[ep] - edgelist[ep];
            //w += connectivity[ep.s, ep.t] - edgelist[ep];
          }

          line = a.articleNo.ToString() + ",";
          line += a.numNodes.ToString() + ",";
          line += d1.ToString() + "," + d2.ToString() + "," + d3.ToString() + ",";
          line += a.depth.ToString() + ",";
          line += (a.numNodes - leaf).ToString() + ",";
          line += a.participants.Count.ToString() + ",";
          line += w.ToString() + ",";
          line += e.ToString();
          sw.WriteLine(line);
        }
        sw.Close();
      }
    }
    #endregion
    #endregion

    #region Ariticle Level Analysis

    #region Article List Select, Article Info Fileout
    private void articleListCb_SelectedIndexChanged(object sender, EventArgs e) {
      Dictionary<int, double> temperature = new Dictionary<int, double>();      
      BuildArticleTree(articleListCb.SelectedIndex);
      DrawGraph(graphControl1, new TreeLayouter());
      
      /* TODO : 각 글마다의 유저 네트워크 그리기
      BuildArticleNetwork(articleListCb.SelectedIndex);
      DrawGraph(graphControl2, new CircularLayouter());
      */
      CalcTempSeries(articleList[articleListCb.SelectedIndex], ref temperature);
      DrawTempSeries(ref temperature);

      radioTree.Checked = true;

      numericC0.Enabled = true;
      numericC1.Enabled = true;
      numericK.Enabled = true;
      numericDecRat.Enabled = true;
      numericDeadDuration.Enabled = true;
      btnTempParamDefault.Enabled = true;
      numericFocused.Enabled = true;

      FillArticleInfo();
    }
    private void FillArticleInfo() {
      Article a = articleList[articleListCb.SelectedIndex];
      string line = "";

      labelArticleInfo.Text = a.articleNo.ToString();
      line = a.writer.id.ToString();
      line += "(" + userList[a.writer.id].nickname + ", ";
      line += (userList[a.writer.id].num_article_actual.Count() / 100.0).ToString() + "%)";
      labelWriterInfo.Text = line;
      labelViewInfo.Text = a.view.ToString();
      labelNumReplyInfo.Text = a.replyList.Count.ToString();
      labelNumPartInfo.Text = a.participants.Count.ToString();
      
      labelAvgDepthInfo.Text = a.avgDepth.ToString();
      labelDepthInfo.Text = a.depth.ToString();      
      labelDepthOneInfo.Text = a.replyOnceCount.ToString();
      labelMDegreeInfo.Text = a.maxDegree.ToString();
      labelNumLeafInfo.Text = a.numLeaf.ToString();

      labelLifetimeInfo.Text = a.lifetime.ToString();
      labelTempsumInfo.Text = a.tempSum.ToString();
      labelIPLInfo.Text = a.ipl.ToString();
      labelXPLInfo.Text = a.xpl.ToString();
      labelTempPeakInfo.Text = a.tempPeak.ToString();
      labelTempPeakTimeInfo.Text = a.tempPeakTime.ToString();

      line = a.mostReplier;
      line += "(" + userList[a.mostReplier].nickname + ", ";
      line += a.participants[a.mostReplier].ToString() + ")";
      labelTopReplierInfo.Text = line;
    }
    #endregion

    #region Build Article Tree    
    //TODO : 지금은 글쓴이를 포함해서 그리도록 함
    private void BuildArticleTree(int idx) {
      int i;
      string labelText;
      ReplyNode p, c;
      Queue<INode> nodeQ = new Queue<INode>();
      Queue<ReplyNode> userQ = new Queue<ReplyNode>();
      List<INode> nodeList = new List<INode>();
      List<IEdge> edgeList = new List<IEdge>();
      INode pn, cn;
      double fv = 0;
      ShapeNodeStyle sns = new ShapeNodeStyle ( ShapeNodeShape.Ellipse, Pens.Black, Brushes.Yellow );

      //TODO : 노드의 focused 정도 시각화
      //1. 일정 이상이면 다 출력하기
      //2. 가장 큰 것도 출력하기
      //3. focus 정도에 따라서 색상 지정하기

      graphControl1.Graph.Clear();
      
      Article a = articleList[idx];

      p = a.writer;
      pn = graphControl1.Graph.CreateNode(new RectD(0, 0, 100, 100));

      labelText =
        "(writer)" + p.id.ToString() + Environment.NewLine + 
        p.timeInfo.timeStamp.ToShortDateString() + Environment.NewLine + 
        p.timeInfo.timeStamp.ToShortTimeString();
      graphControl1.Graph.AddLabel(pn, labelText);

      userQ.Enqueue(p);
      nodeQ.Enqueue(pn);

      while (userQ.Count > 0) {
        p = userQ.Dequeue();
        pn = nodeQ.Dequeue();
        
        for (i = 0; i < p.children.Count; i++) {
          c = p.children[i];
          if (c.depth == 1 && c.children.Count() < Constants.CHILD_THRESHOLD) continue;          
          cn = graphControl1.Graph.CreateNode(new RectD(0, 0, 100, 100));

          fv = c.FocusedValue();
          if (fv > (double)numericFocused.Value) graphControl1.Graph.SetStyle(cn, sns);

          //TODO : focus value가 일정 이상이면 색깔을 주도록 하자
          labelText =
            c.id.ToString() + Environment.NewLine +
            c.timeInfo.timeStamp.ToShortDateString() + Environment.NewLine +
            c.timeInfo.timeStamp.ToShortTimeString() + Environment.NewLine +
            c.TopTwoEnthusiasticParticipant();
          graphControl1.Graph.AddLabel(cn, labelText);

          graphControl1.Graph.CreateEdge(pn, cn);

          userQ.Enqueue(c);
          nodeQ.Enqueue(cn);
        }
      }
    }
    #endregion
    
    #region Build Article Network
    //TODO : 개별 글에 대한 네트워크 생성
    //TODO : 등장하는 에지 pair 다 확인
    //TODO : dictionary 생성    
    private void BuildArticleNetwork(int idx) {

    }
    #endregion

    #region save article temperature
    private void saveArticleTempBtn_Click(object sender, EventArgs e) {
      cp.PrintTemperature(articleList[articleListCb.SelectedIndex], (int)numericK.Value,
        (int)numericC0.Value, (int)numericC1.Value, (double)numericDecRat.Value);
    }

    #endregion save article temperature

    #region set article temerature chart
    private void CalcTempSeries(Article a, ref Dictionary<int, double> temperature) {
      int i, replyCursor = 0;
      int dead_last = 0;

      double tmprt;
      double tempPeak;
      int tempPeakTime;

      bool NOT_DEAD = true;
      double tempsum = 0;

      int k = (int)numericK.Value;
      int c0 = (int)numericC0.Value;
      int c1 = (int)numericC1.Value;
      double decRat = (double)numericDecRat.Value;
      int deadDuration = (int)numericDeadDuration.Value;
      
      //초기화
      temperature[0] = a.InitialTemperature();
      while (replyCursor < a.replyList.Count() && a.ReplyIndex(replyCursor) == 0) {
        temperature[0] += a.replyList[replyCursor].Temperature(k, c0, c1);
        replyCursor++;
      }
      i = 1;
      tempsum = temperature[0];
      tempPeak = temperature[0];
      tempPeakTime = 0;

      while (NOT_DEAD) {
        //일단 온도 설정
        tmprt = temperature[i - 1] * decRat;

        //온도가 임계 미만이면 종료 카운트 시작함
        if (tmprt < Constants.TEMP_DEAD_THRESHOLD) {
          temperature[i] = temperature[i - 1];
          dead_last++;
        }
        else {
          temperature[i] = tmprt;
          dead_last = 0;
        }

        //같은 시각인 댓글들을 온도에 더해줌
        while (replyCursor < a.replyList.Count() && a.ReplyIndex(replyCursor) == i) {
          temperature[i] += a.replyList[replyCursor].Temperature(k, c0, c1);
          replyCursor++;
        }

        if (temperature[i] > tempPeak) {
          tempPeak = temperature[i];
          tempPeakTime = i;
        }
        tempsum += temperature[i];        

        if (dead_last > deadDuration * 60) NOT_DEAD = false;
        i++;
      }
      a.tempSum = tempsum;
      a.tempPeak = tempPeak;
      a.tempPeakTime = tempPeakTime;

      a.lifetime = i;
    }

    private void DrawTempSeries(ref Dictionary<int, double> temperature) {
      ChartDataSeries cs;
      cs = chartTemperature.ChartGroups.Group0.ChartData.SeriesList[0];
      cs.X.CopyDataIn(temperature.Keys.ToArray());
      cs.Y.CopyDataIn(temperature.Values.ToArray());
    }
    #endregion set article temerature chart

    #endregion Ariticle Level Analysis

    #region Board Level Analysis

    #region UserNet Button
    private void userNetworkBtn_Click(object sender, EventArgs ea) {
      BuildUserNetwork();
      DrawGraph(graphControl1, new CircularLayouter());
      radioCircular.Checked = true;
    }
    #endregion

    #region User Connectivity Calc, User Network Build
    private void CalcUserConnectivity() {
      int i, j;
      ReplyNode p, c;
      Queue<ReplyNode> userQ = new Queue<ReplyNode>();
      EdgePair e;

      graphControl1.Graph.Clear();
      for (i = 0; i < articleList.Count; i++) {
        Article a = articleList[i];
        p = a.writer; //root
        userQ.Enqueue(p);

        while (userQ.Count > 0) {
          p = userQ.Dequeue();
          for (j = 0; j < p.children.Count; j++) {
            c = p.children[j];
            userQ.Enqueue(c);
            
            //깊이 얼마 이상부터 의미가 있다고 판단
            if (c.depth >= Constants.USER_NET_DEPTH_THRESHOLD) {
              e = new EdgePair(int.Parse(p.id), int.Parse(c.id));
              if(conn.ContainsKey(e)) {
                conn[e] += 1;
              }
              else {
                conn[e] = 1;
              }              
            }
          }
        }
      }

      logText.AppendText(Environment.NewLine + "Connectivity Calculation Finished");
    }

    //일단 연결 정보에 따라서 모든 노드, 에지 생성
    //제공 알고리즘으로 component 확인해서 
    private void BuildUserNetwork() {
      int i, j, edgeThreshold;
      INode s,t;
      IEdge e;
      Dictionary<int, INode> nodeList = new Dictionary<int, INode>();
      Pen pen;
      AbstractEdgeStyle a = new PolylineEdgeStyle();

      graphControl1.Graph.Clear();
      edgeThreshold = (int)numericEdgeThreshold.Value;
      
      ///dictionary를 사용하면 양방향 처리는 어떻게 할 수 있는지?
      ///에지 목록이 있으면? 이전 에지를 bidirectional로 변경하면 되는지?
      ///각각 하는 것이 제일 편할 것 같기는 함..
      ///일단은 다 만들어놓고 디그리가 낮은 노드를 지우도록 하자
      
      ///TODO : 그래프에서 에지가 두 개 이상이면 노드를 옮겼을 때 에지가 같이 움직이지 않음
      ///그래서 에지는 항상 하나만 유지하도록 했으면 좋겠다
      foreach(EdgePair ep in conn.Keys) {
        if(conn[ep] > edgeThreshold) {
          if (nodeList.ContainsKey(ep.s)) s = nodeList[ep.s];
          else s = graphControl1.Graph.CreateNode();
          graphControl1.Graph.AddLabel(s, ep.s.ToString());
          nodeList[ep.s] = s;

          if (nodeList.ContainsKey(ep.t)) t = nodeList[ep.t];
          else t = graphControl1.Graph.CreateNode();
          graphControl1.Graph.AddLabel(t, ep.t.ToString());
          nodeList[ep.t] = t;

          e = graphControl1.Graph.CreateEdge(s, t);
          pen = new Pen(Brushes.Black, 1);
          pen.Width = conn[ep] * (float)0.1;

          pen.Color = EDGE_NORMAL;
          a = new PolylineEdgeStyle(pen);
          a.TargetArrow = DefaultArrow.Triangle;
          
          graphControl1.Graph.SetStyle(e, a);
        }
      }
      
      ///전체 연결된 그래프에서 상위 몇 개의 컴포넌트만 살리고 나머지 죽임
      ///TODO : 컴포넌트 크기 순으로 내림차순 정렬
      ///지금은 맨 처음 것만 남기고 다 죽이므로 괜찮은데,
      ///만약 컴포넌트들이 여러 개의 큰 덩어리로 나뉘어질 경우는 정렬이 필요함      
      YGraphAdapter ga = new YGraphAdapter(graphControl1.Graph);
      if (!GraphConnectivity.IsConnected(ga.YGraph)) {
        INodeMap components = ga.YGraph.CreateNodeMap();
        NodeList[] nl = GraphConnectivity.ConnectedComponents(ga.YGraph);

        for (i = Constants.COMPO_DISCARD_THRESHOLD; i < nl.Count(); i++) {
          INodeCursor nc = nl[i].Nodes();
          while (nc.Ok) {
            graphControl1.Graph.RemoveNode(ga.GetOriginalNode(nc.Node));
            ga.YGraph.Hide(nc.Node);
            nc.Next();
          }
        }
      }
    }

    #endregion

    #endregion

    private void numericSymbolSize_ValueChanged(object sender, EventArgs e) {
      chartTemperature.ChartGroups.Group0.ChartData.SeriesList[0].SymbolStyle.Size =
        (int)numericSymbolSize.Value;
    }

    private void numericLineSize_ValueChanged(object sender, EventArgs e) {
      chartTemperature.ChartGroups.Group0.ChartData.SeriesList[0].LineStyle.Thickness =
        (int)numericLineSize.Value;
    }

    //지금 아티클 목록에서 선택된 글의 온도 그래프를 변경된 계수로 다시 그림
    //
    #region TempChart Param Numerics
    private void numericK_ValueChanged(object sender, EventArgs e) {
      ChartParameterChangedProcedure();
    }
    private void numericC0_ValueChanged(object sender, EventArgs e) {
      ChartParameterChangedProcedure();
    }
    private void numericC1_ValueChanged(object sender, EventArgs e) {
      ChartParameterChangedProcedure();
    }
    private void numericDecRat_ValueChanged(object sender, EventArgs e) {
      ChartParameterChangedProcedure();
    }
    private void numericDeadDuration_ValueChanged(object sender, EventArgs e) {
      ChartParameterChangedProcedure();
    }
    #endregion TempChart Param Numerics

    public void ChartParameterChangedProcedure() {
      Dictionary<int, double> temperature = new Dictionary<int, double>();
      CalcTempSeries(articleList[articleListCb.SelectedIndex], ref temperature);
      DrawTempSeries(ref temperature);
      FillArticleInfo();
    }

    private void userFileoutBtn_Click(object sender, EventArgs e) {

    }

    private void btnTempParamDefault_Click(object sender, EventArgs e) {
      numericK.Value = 100;      
      numericC0.Value = 1000;
      numericC1.Value = 10;
      numericDecRat.Value = 0.99M;
      numericDeadDuration.Value = 24;
    }

    private void BuildArticleSubtrees(int idx) {
      articleList[idx].ConstructSubtreeInfo();
    }

    private void btnSubtree_Click(object sender, EventArgs e) {
      int i = 0;
      BuildArticleTree(0);
      DrawGraph(graphControl1, new TreeLayouter());
      articleList[0].ConstructSubtreeInfo();
      i = 1;
    }

    private void numericFocused_ValueChanged(object sender, EventArgs e) {

    }
  }
}
