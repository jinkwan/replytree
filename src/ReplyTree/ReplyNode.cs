﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class ReplyNode
  {
    public string id { get; set; }
    public ReplyNode parent { get; set; }
    public List<ReplyNode> children { get; }
    public int contentLength { get; }
    public int depth { get; }
    public ReplyTimeInfo timeInfo { get; set; }
    public Dictionary<int, int> subtreeInfo;
       

    /*
    1. subtree 정보는 갖고 있음
    2. 정렬된 subtree 정보를 가질지?
    
    */


    public ReplyNode(string id, ReplyNode parent, int contentLength, int depth, ReplyTimeInfo time) {
      this.id = id;
      this.parent = parent;
      this.children = new List<ReplyNode>();
      this.contentLength = contentLength;
      this.depth = depth;
      this.timeInfo = time;
      subtreeInfo = new Dictionary<int, int>();
      subtreeInfo[int.Parse(this.id)] = 1;
    }

    public double Temperature(int k, int c0, int c1) {
      double result = 0;

      result = (contentLength + c1) * k / (Math.Pow((this.timeInfo - parent.timeInfo), 2) + c0);

      //sqrt(현재댓글 사이즈 + 상수1) / (parent 댓글 시간-현재댓글 시간)
      /*
      result = (contentLength + Constants.TEMP_REPLY_MODULEE)
        / Math.Sqrt(this.timeInfo - parent.timeInfo + Constants.TEMP_REPLY_MODULAR);
      */
      return result;
    }

    public void AddChild(ReplyNode child) {
      this.children.Add(child);
    }
    public static bool operator <(ReplyNode r1, ReplyNode r2) {
      if (r1.timeInfo.timeStamp < r2.timeInfo.timeStamp) return true;
      else return false;
    }
    public static bool operator >(ReplyNode r1, ReplyNode r2) {
      if (r1.timeInfo.timeStamp > r2.timeInfo.timeStamp) return true;
      else return false;
    }
    public Dictionary<int, int> GetSubtree() {
      Dictionary<int, int> childSubtreeInfo = new Dictionary<int, int>();
      
      foreach(ReplyNode n in children) {
        childSubtreeInfo = n.GetSubtree();
        foreach(int nid in n.subtreeInfo.Keys) {
          if(subtreeInfo.ContainsKey(nid)) {
            subtreeInfo[nid] += n.subtreeInfo[nid];
          }
          else {
            subtreeInfo[nid] = n.subtreeInfo[nid];
          }
        }
      }

      var items = from pair in subtreeInfo orderby pair.Value descending select pair;
      subtreeInfo = items.ToDictionary(x => x.Key, x => x.Value);      
      return subtreeInfo;
    }

    public int NumSubtreeNodes() {
      return subtreeInfo.Sum(x => x.Value);
    }

    public double FocusedValue() {
      if(subtreeInfo.Count < 2) {
        return (double)subtreeInfo.ElementAt(0).Value / (NumSubtreeNodes() + Constants.FOCUS_CONSTANT);
      }
      else {
        return ((double)subtreeInfo.ElementAt(0).Value + subtreeInfo.ElementAt(1).Value) / (NumSubtreeNodes() + Constants.FOCUS_CONSTANT);
      }
    }

    public string TopTwoEnthusiasticParticipant() {
      string result = "";
      int t1 = subtreeInfo.ElementAt(0).Value;
      int t2;
      double calc = 0;

      if(subtreeInfo.Count < 2) {
        calc = (double)t1 / (NumSubtreeNodes() + Constants.FOCUS_CONSTANT);
        result += t1.ToString() + "/(" + NumSubtreeNodes().ToString() + "+";
        result += Constants.FOCUS_CONSTANT.ToString() + ")=";
        result += Environment.NewLine + calc.ToString("f2");
      }
      else {
        t2 = subtreeInfo.ElementAt(1).Value;
        calc = ((double)t1 + t2) / (NumSubtreeNodes() + Constants.FOCUS_CONSTANT);
        result += "(" + t1.ToString() + "+" + t2.ToString() + ")/";
        result += "(" + NumSubtreeNodes().ToString() + "+";
        result += Constants.FOCUS_CONSTANT.ToString() + ")=";
        result += Environment.NewLine + calc.ToString("f2");
      }
      return result;
    }
  }
}
