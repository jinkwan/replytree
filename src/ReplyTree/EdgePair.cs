﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  public class EdgePair
  {
    public int s { get; set; }
    public int t { get; set; }

    public EdgePair(int s, int t) {
      this.s = s;
      this.t = t;
    }
  }
}
