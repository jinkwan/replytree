﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class UserInfo
  {
    public string id { get; }
    public string nickname { get; }

    //info file 기준
    public int num_article_info { get; set; }
    public int num_reply_info { get; set; }

    //article file 기준
    //<작성글, 얻은 댓글수>, <댓글쓴 글, 쓴 댓글 개수>
    public Dictionary<int, int> num_article_actual = new Dictionary<int, int>();
    public Dictionary<int, int> num_reply_actual = new Dictionary<int, int>();
    //public int acquiredReplies { get; set; }
    
    public UserInfo(string id, string nickname, int articles, int replies) {
      this.id = id;
      this.nickname = nickname;
      num_article_info = articles;
      num_reply_info = replies;
    }

    public int AcquiredReplies() {
      int total = 0;
      foreach(int replies in num_article_actual.Values) {
        total += replies;
      }
      return total;
    }

    public int TotalReply() {
      int total = 0;
      foreach(int replies in num_reply_actual.Values) {
        total += replies;
      }
      return total;
    }
  }
}
