﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class Article {
    public int articleNo { get; }
    public ReplyNode writer { get; }        
    public int view { get; set; }
    public int numNodes { get; set; }
    public Dictionary<string, int> participants = new Dictionary<string, int>();

    public double avgDepth { get; set; }
    public int depth { get; set; }
    public int replyOnceCount { get; set; }
    public int maxDegree { get; set; }
    public int numLeaf { get; set; }

    public int lifetime { get; set; }
    public double tempSum { get; set; }
    public int ipl { get; set; }
    public int xpl { get; set; }

    public double tempPeak { get; set; }
    public int tempPeakTime { get; set; }
    
    public ReplyNode lastReply { get; set; }    
    public string mostReplier { get; set; }
    public List<ReplyNode> replyList = new List<ReplyNode>();


    /*
    1. 글에서 가장 dense한 subtree root를 찾기 - 분모에 글의 노드수 전체를 더해주면 될 듯
    2. 만약에 글에서 일정 이상으로 dense한 부분들을 모두 찾고 싶다면 분모에 더하는 값은 constant로 주고,
    dense judge threshold를 설정해두면 될 것
    */
    

    ///TODO : article에서 에지 목록을 갖는 것도 도움이 될 것 같음
    ///나중에 그래프에서 어떤 에지(interaction)을 발견했을 때,
    ///해당 에지가 어떤 다른 글에서 또 발생했는지를 확인할 수 있을 것

    public Article(int no, int view, ReplyNode writer) {
      this.articleNo = no;
      this.view = view;
      this.writer = writer;
    }

    /// <summary>
    /// 아티클 깊이, 최대 디그리, 댓글 개수,
    /// 평균 깊이, 리프노드 수, 글의 생명주기
    /// 를 계산하는 함수.
    /// </summary>
    public void CalcTreeInfo() {
      int i;      
      int md=0, d=0, leaf=0, ipl=0, xpl=0; //최대디그리, 뎊스, 리프수, ipl, xpl
      double ad = 0; //평균뎊스
      Queue<ReplyNode> userList = new Queue<ReplyNode>();
      ReplyNode u;
      ReplyNode last;

      last = writer;

      for(i=0;i<writer.children.Count;i++) {
        userList.Enqueue(writer.children[i]);
      }
      //userList.Enqueue(writer);
      while(userList.Count > 0) {
        u = userList.Dequeue();

        ad += u.depth;
        if (md < u.children.Count) {
          md = u.children.Count;
        }
        if (d < u.depth) d = u.depth;

        if (u.children.Count() == 0) {
          leaf++;
          xpl += u.depth;
        }

        for(i=0;i<u.children.Count;i++) {
          userList.Enqueue(u.children[i]);
        }

        if (u.timeInfo > last.timeInfo) last = u;

        replyList.Add(u);
      }

      depth = d;
      maxDegree = md;
      numNodes = replyList.Count();
      avgDepth = ad / numNodes;
      numLeaf = leaf;
      lastReply = last;
      this.xpl = xpl;
      this.ipl = (int)ad - xpl;
    }

    /// <summary>
    /// 아티클 댓글에 참여한 회원들의 댓글 참여 횟수를 카운트하는 함수.
    /// </summary>
    public void CalcReplyCountInfo() {
      int numOnce=0, numMost=0;
      
      foreach(KeyValuePair<string, int> token in participants) {
        //depth 1 replies
        if (participants[token.Key] == 1) numOnce++;

        //most enthusiastic replier
        if (numMost < participants[token.Key]) {
          numMost = participants[token.Key];
          mostReplier = token.Key;
        }
      }
      replyOnceCount = numOnce;
    }

    public int ArticleLifetime() {
      return lastReply.timeInfo - writer.timeInfo;
    }
    
    public int ReplyIndex(int cursor) {
      return replyList[cursor].timeInfo - writer.timeInfo;
    }

    public void SortReplyListByTimestamp() {
      //1. 모든 노드를 reply list에 입력 - 함
      //2. ReplyTimeComparer로 sort 수행
      replyList.Sort(new ReplyTimeComparer());
    }

    public double InitialTemperature() {
      double temp = 0;

      temp = Constants.TEMP_INIT + Math.Sqrt(writer.contentLength);

      return temp;
    }
    
    //정렬만 하기
    //정렬해서 상위 두 개 반환하기
    //정렬해서 상위 두 개로 계산한 결과 반환하기
    
    public void ConstructSubtreeInfo() {
      writer.GetSubtree();
    }

    public double BestFocusedValue() {
      double bfv = 0;

      //DFS나 BFS로 출력

      return bfv;
    }
  }
}
