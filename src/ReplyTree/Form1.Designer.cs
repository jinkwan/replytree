﻿namespace ReplyTree
{
  partial class Form1
  {
    /// <summary>
    /// 필수 디자이너 변수입니다.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// 사용 중인 모든 리소스를 정리합니다.
    /// </summary>
    /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
    protected override void Dispose(bool disposing) {
      if (disposing && (components != null)) {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form 디자이너에서 생성한 코드

    /// <summary>
    /// 디자이너 지원에 필요한 메서드입니다. 
    /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
    /// </summary>
    private void InitializeComponent() {
      yWorks.Canvas.ViewportLimiter viewportLimiter2 = new yWorks.Canvas.ViewportLimiter();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
      this.loadUserBtn = new System.Windows.Forms.Button();
      this.loadArticleBtn = new System.Windows.Forms.Button();
      this.articleListCb = new System.Windows.Forms.ComboBox();
      this.graphControl1 = new yWorks.yFiles.UI.GraphControl();
      this.logText = new System.Windows.Forms.TextBox();
      this.labelWriter = new System.Windows.Forms.Label();
      this.labelNumPartInfo = new System.Windows.Forms.Label();
      this.labelWriterInfo = new System.Windows.Forms.Label();
      this.labelNumPart = new System.Windows.Forms.Label();
      this.labelAvgDepth = new System.Windows.Forms.Label();
      this.labelAvgDepthInfo = new System.Windows.Forms.Label();
      this.labelTopReplier = new System.Windows.Forms.Label();
      this.labelTopReplierInfo = new System.Windows.Forms.Label();
      this.labelDepth = new System.Windows.Forms.Label();
      this.labelDepthInfo = new System.Windows.Forms.Label();
      this.labelRelatedUsers = new System.Windows.Forms.Label();
      this.labelRelatedUsersInfo = new System.Windows.Forms.Label();
      this.labelMDegree = new System.Windows.Forms.Label();
      this.labelMDegreeInfo = new System.Windows.Forms.Label();
      this.labelDepthOne = new System.Windows.Forms.Label();
      this.labelDepthOneInfo = new System.Windows.Forms.Label();
      this.userNetworkBtn = new System.Windows.Forms.Button();
      this.radioCircular = new System.Windows.Forms.RadioButton();
      this.radioHierachical = new System.Windows.Forms.RadioButton();
      this.radioOrthogonal = new System.Windows.Forms.RadioButton();
      this.radioTree = new System.Windows.Forms.RadioButton();
      this.labelGraphLayoutSelector = new System.Windows.Forms.Label();
      this.radioOrganic = new System.Windows.Forms.RadioButton();
      this.imgSaveBtn = new System.Windows.Forms.Button();
      this.labelLoad = new System.Windows.Forms.Label();
      this.labelFileout = new System.Windows.Forms.Label();
      this.numericEdgeThreshold = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.labelGraphSave = new System.Windows.Forms.Label();
      this.labelNode = new System.Windows.Forms.Label();
      this.labelParticipants = new System.Windows.Forms.Label();
      this.labelView = new System.Windows.Forms.Label();
      this.labelArticleNo = new System.Windows.Forms.Label();
      this.numericNodeThreshold = new System.Windows.Forms.NumericUpDown();
      this.numericDepthThreshold = new System.Windows.Forms.NumericUpDown();
      this.labelNodeThrsDiff = new System.Windows.Forms.Label();
      this.labelDepthThrs = new System.Windows.Forms.Label();
      this.labelPortionThreshold = new System.Windows.Forms.Label();
      this.numericRatio = new System.Windows.Forms.NumericUpDown();
      this.labelWriterReply = new System.Windows.Forms.Label();
      this.btnMatView = new System.Windows.Forms.Button();
      this.labelArticleMat = new System.Windows.Forms.Label();
      this.btnMatReply = new System.Windows.Forms.Button();
      this.articleFileoutBtn = new System.Windows.Forms.Button();
      this.saveArticleTempBtn = new System.Windows.Forms.Button();
      this.userFileoutBtn = new System.Windows.Forms.Button();
      this.labelViews = new System.Windows.Forms.Label();
      this.labelViewInfo = new System.Windows.Forms.Label();
      this.labelNumReplyInfo = new System.Windows.Forms.Label();
      this.labelNumReply = new System.Windows.Forms.Label();
      this.labelArticleInfo = new System.Windows.Forms.Label();
      this.labelArticle = new System.Windows.Forms.Label();
      this.chartTemperature = new C1.Win.C1Chart.C1Chart();
      this.numericSymbolSize = new System.Windows.Forms.NumericUpDown();
      this.numericLineSize = new System.Windows.Forms.NumericUpDown();
      this.labelLine = new System.Windows.Forms.Label();
      this.labelSymbol = new System.Windows.Forms.Label();
      this.numericK = new System.Windows.Forms.NumericUpDown();
      this.numericC0 = new System.Windows.Forms.NumericUpDown();
      this.numericC1 = new System.Windows.Forms.NumericUpDown();
      this.labelK = new System.Windows.Forms.Label();
      this.labelC0 = new System.Windows.Forms.Label();
      this.labelC1 = new System.Windows.Forms.Label();
      this.labelDecRat = new System.Windows.Forms.Label();
      this.numericDecRat = new System.Windows.Forms.NumericUpDown();
      this.labelDeadDur = new System.Windows.Forms.Label();
      this.numericDeadDuration = new System.Windows.Forms.NumericUpDown();
      this.labelNumLeaf = new System.Windows.Forms.Label();
      this.labelNumLeafInfo = new System.Windows.Forms.Label();
      this.labelLifetime = new System.Windows.Forms.Label();
      this.labelLifetimeInfo = new System.Windows.Forms.Label();
      this.labelTempsum = new System.Windows.Forms.Label();
      this.labelTempsumInfo = new System.Windows.Forms.Label();
      this.labelIPL = new System.Windows.Forms.Label();
      this.labelIPLInfo = new System.Windows.Forms.Label();
      this.labelXPL = new System.Windows.Forms.Label();
      this.labelXPLInfo = new System.Windows.Forms.Label();
      this.labelTempPeak = new System.Windows.Forms.Label();
      this.labelTempPeakInfo = new System.Windows.Forms.Label();
      this.btnTempParamDefault = new System.Windows.Forms.Button();
      this.labelTempPeakTime = new System.Windows.Forms.Label();
      this.labelTempPeakTimeInfo = new System.Windows.Forms.Label();
      this.btnSubtree = new System.Windows.Forms.Button();
      this.labelFocusThrs = new System.Windows.Forms.Label();
      this.numericFocused = new System.Windows.Forms.NumericUpDown();
      ((System.ComponentModel.ISupportInitialize)(this.numericEdgeThreshold)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericNodeThreshold)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDepthThreshold)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericRatio)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.chartTemperature)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericSymbolSize)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericLineSize)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericK)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericC0)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericC1)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDecRat)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDeadDuration)).BeginInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericFocused)).BeginInit();
      this.SuspendLayout();
      // 
      // loadUserBtn
      // 
      this.loadUserBtn.Location = new System.Drawing.Point(12, 34);
      this.loadUserBtn.Name = "loadUserBtn";
      this.loadUserBtn.Size = new System.Drawing.Size(75, 23);
      this.loadUserBtn.TabIndex = 0;
      this.loadUserBtn.Text = "User";
      this.loadUserBtn.UseVisualStyleBackColor = true;
      this.loadUserBtn.Click += new System.EventHandler(this.loadUserBtn_Click);
      // 
      // loadArticleBtn
      // 
      this.loadArticleBtn.Location = new System.Drawing.Point(93, 34);
      this.loadArticleBtn.Name = "loadArticleBtn";
      this.loadArticleBtn.Size = new System.Drawing.Size(75, 23);
      this.loadArticleBtn.TabIndex = 1;
      this.loadArticleBtn.Text = "Article";
      this.loadArticleBtn.UseVisualStyleBackColor = true;
      this.loadArticleBtn.Click += new System.EventHandler(this.loadArticleBtn_Click);
      // 
      // articleListCb
      // 
      this.articleListCb.FormattingEnabled = true;
      this.articleListCb.Location = new System.Drawing.Point(12, 270);
      this.articleListCb.Name = "articleListCb";
      this.articleListCb.Size = new System.Drawing.Size(437, 20);
      this.articleListCb.TabIndex = 2;
      this.articleListCb.SelectedIndexChanged += new System.EventHandler(this.articleListCb_SelectedIndexChanged);
      // 
      // graphControl1
      // 
      this.graphControl1.BackColor = System.Drawing.Color.White;
      this.graphControl1.ContentRect = new yWorks.Canvas.Geometry.Structs.RectD(0D, 0D, 400D, 400D);
      this.graphControl1.FitContentViewMargins = new yWorks.Canvas.Geometry.Structs.InsetsD(10D, 10D, 10D, 10D);
      this.graphControl1.Location = new System.Drawing.Point(455, 0);
      this.graphControl1.Name = "graphControl1";
      this.graphControl1.Size = new System.Drawing.Size(1149, 498);
      this.graphControl1.TabIndex = 3;
      this.graphControl1.Text = "graphControl1";
      viewportLimiter2.Bounds = null;
      this.graphControl1.ViewportLimiter = viewportLimiter2;
      // 
      // logText
      // 
      this.logText.Enabled = false;
      this.logText.Location = new System.Drawing.Point(12, 857);
      this.logText.Multiline = true;
      this.logText.Name = "logText";
      this.logText.ReadOnly = true;
      this.logText.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
      this.logText.Size = new System.Drawing.Size(437, 103);
      this.logText.TabIndex = 4;
      this.logText.Text = "Log";
      // 
      // labelWriter
      // 
      this.labelWriter.AutoSize = true;
      this.labelWriter.BackColor = System.Drawing.Color.Transparent;
      this.labelWriter.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelWriter.ForeColor = System.Drawing.Color.Gray;
      this.labelWriter.Location = new System.Drawing.Point(12, 422);
      this.labelWriter.Name = "labelWriter";
      this.labelWriter.Size = new System.Drawing.Size(64, 22);
      this.labelWriter.TabIndex = 5;
      this.labelWriter.Text = "Writer";
      // 
      // labelNumPartInfo
      // 
      this.labelNumPartInfo.AutoSize = true;
      this.labelNumPartInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelNumPartInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumPartInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelNumPartInfo.Location = new System.Drawing.Point(150, 488);
      this.labelNumPartInfo.Name = "labelNumPartInfo";
      this.labelNumPartInfo.Size = new System.Drawing.Size(222, 22);
      this.labelNumPartInfo.TabIndex = 8;
      this.labelNumPartInfo.Text = "#participants(w/o writer)";
      // 
      // labelWriterInfo
      // 
      this.labelWriterInfo.AutoSize = true;
      this.labelWriterInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelWriterInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelWriterInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelWriterInfo.Location = new System.Drawing.Point(150, 422);
      this.labelWriterInfo.Name = "labelWriterInfo";
      this.labelWriterInfo.Size = new System.Drawing.Size(154, 22);
      this.labelWriterInfo.TabIndex = 9;
      this.labelWriterInfo.Text = "id(name), portion";
      // 
      // labelNumPart
      // 
      this.labelNumPart.AutoSize = true;
      this.labelNumPart.BackColor = System.Drawing.Color.Transparent;
      this.labelNumPart.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumPart.ForeColor = System.Drawing.Color.Gray;
      this.labelNumPart.Location = new System.Drawing.Point(12, 488);
      this.labelNumPart.Name = "labelNumPart";
      this.labelNumPart.Size = new System.Drawing.Size(119, 22);
      this.labelNumPart.TabIndex = 10;
      this.labelNumPart.Text = "#Participants";
      // 
      // labelAvgDepth
      // 
      this.labelAvgDepth.AutoSize = true;
      this.labelAvgDepth.BackColor = System.Drawing.Color.Transparent;
      this.labelAvgDepth.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelAvgDepth.ForeColor = System.Drawing.Color.Gray;
      this.labelAvgDepth.Location = new System.Drawing.Point(12, 527);
      this.labelAvgDepth.Name = "labelAvgDepth";
      this.labelAvgDepth.Size = new System.Drawing.Size(93, 22);
      this.labelAvgDepth.TabIndex = 12;
      this.labelAvgDepth.Text = "Avg.Depth";
      // 
      // labelAvgDepthInfo
      // 
      this.labelAvgDepthInfo.AutoSize = true;
      this.labelAvgDepthInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelAvgDepthInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelAvgDepthInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelAvgDepthInfo.Location = new System.Drawing.Point(150, 527);
      this.labelAvgDepthInfo.Name = "labelAvgDepthInfo";
      this.labelAvgDepthInfo.Size = new System.Drawing.Size(124, 22);
      this.labelAvgDepthInfo.TabIndex = 11;
      this.labelAvgDepthInfo.Text = "average depth";
      // 
      // labelTopReplier
      // 
      this.labelTopReplier.AutoSize = true;
      this.labelTopReplier.BackColor = System.Drawing.Color.Transparent;
      this.labelTopReplier.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTopReplier.ForeColor = System.Drawing.Color.Gray;
      this.labelTopReplier.Location = new System.Drawing.Point(12, 810);
      this.labelTopReplier.Name = "labelTopReplier";
      this.labelTopReplier.Size = new System.Drawing.Size(99, 22);
      this.labelTopReplier.TabIndex = 14;
      this.labelTopReplier.Text = "TopReplier";
      // 
      // labelTopReplierInfo
      // 
      this.labelTopReplierInfo.AutoSize = true;
      this.labelTopReplierInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelTopReplierInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTopReplierInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelTopReplierInfo.Location = new System.Drawing.Point(150, 810);
      this.labelTopReplierInfo.Name = "labelTopReplierInfo";
      this.labelTopReplierInfo.Size = new System.Drawing.Size(160, 22);
      this.labelTopReplierInfo.TabIndex = 13;
      this.labelTopReplierInfo.Text = "id(name), #replies";
      // 
      // labelDepth
      // 
      this.labelDepth.AutoSize = true;
      this.labelDepth.BackColor = System.Drawing.Color.Transparent;
      this.labelDepth.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDepth.ForeColor = System.Drawing.Color.Gray;
      this.labelDepth.Location = new System.Drawing.Point(13, 549);
      this.labelDepth.Name = "labelDepth";
      this.labelDepth.Size = new System.Drawing.Size(59, 22);
      this.labelDepth.TabIndex = 16;
      this.labelDepth.Text = "Depth";
      // 
      // labelDepthInfo
      // 
      this.labelDepthInfo.AutoSize = true;
      this.labelDepthInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelDepthInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDepthInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelDepthInfo.Location = new System.Drawing.Point(151, 549);
      this.labelDepthInfo.Name = "labelDepthInfo";
      this.labelDepthInfo.Size = new System.Drawing.Size(113, 22);
      this.labelDepthInfo.TabIndex = 15;
      this.labelDepthInfo.Text = "depth of tree";
      // 
      // labelRelatedUsers
      // 
      this.labelRelatedUsers.AutoSize = true;
      this.labelRelatedUsers.BackColor = System.Drawing.Color.Transparent;
      this.labelRelatedUsers.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelRelatedUsers.ForeColor = System.Drawing.Color.Gray;
      this.labelRelatedUsers.Location = new System.Drawing.Point(12, 832);
      this.labelRelatedUsers.Name = "labelRelatedUsers";
      this.labelRelatedUsers.Size = new System.Drawing.Size(71, 22);
      this.labelRelatedUsers.TabIndex = 18;
      this.labelRelatedUsers.Text = "Related";
      // 
      // labelRelatedUsersInfo
      // 
      this.labelRelatedUsersInfo.AutoSize = true;
      this.labelRelatedUsersInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelRelatedUsersInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelRelatedUsersInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelRelatedUsersInfo.Location = new System.Drawing.Point(150, 832);
      this.labelRelatedUsersInfo.Name = "labelRelatedUsersInfo";
      this.labelRelatedUsersInfo.Size = new System.Drawing.Size(48, 22);
      this.labelRelatedUsersInfo.TabIndex = 17;
      this.labelRelatedUsersInfo.Text = "{ids}";
      // 
      // labelMDegree
      // 
      this.labelMDegree.AutoSize = true;
      this.labelMDegree.BackColor = System.Drawing.Color.Transparent;
      this.labelMDegree.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelMDegree.ForeColor = System.Drawing.Color.Gray;
      this.labelMDegree.Location = new System.Drawing.Point(13, 593);
      this.labelMDegree.Name = "labelMDegree";
      this.labelMDegree.Size = new System.Drawing.Size(87, 22);
      this.labelMDegree.TabIndex = 20;
      this.labelMDegree.Text = "M.Degree";
      // 
      // labelMDegreeInfo
      // 
      this.labelMDegreeInfo.AutoSize = true;
      this.labelMDegreeInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelMDegreeInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelMDegreeInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelMDegreeInfo.Location = new System.Drawing.Point(151, 593);
      this.labelMDegreeInfo.Name = "labelMDegreeInfo";
      this.labelMDegreeInfo.Size = new System.Drawing.Size(103, 22);
      this.labelMDegreeInfo.TabIndex = 19;
      this.labelMDegreeInfo.Text = "Max degree";
      // 
      // labelDepthOne
      // 
      this.labelDepthOne.AutoSize = true;
      this.labelDepthOne.BackColor = System.Drawing.Color.Transparent;
      this.labelDepthOne.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDepthOne.ForeColor = System.Drawing.Color.Gray;
      this.labelDepthOne.Location = new System.Drawing.Point(13, 571);
      this.labelDepthOne.Name = "labelDepthOne";
      this.labelDepthOne.Size = new System.Drawing.Size(125, 22);
      this.labelDepthOne.TabIndex = 22;
      this.labelDepthOne.Text = "#Depth1Node";
      // 
      // labelDepthOneInfo
      // 
      this.labelDepthOneInfo.AutoSize = true;
      this.labelDepthOneInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelDepthOneInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDepthOneInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelDepthOneInfo.Location = new System.Drawing.Point(151, 571);
      this.labelDepthOneInfo.Name = "labelDepthOneInfo";
      this.labelDepthOneInfo.Size = new System.Drawing.Size(162, 22);
      this.labelDepthOneInfo.TabIndex = 21;
      this.labelDepthOneInfo.Text = "#depth one replies";
      // 
      // userNetworkBtn
      // 
      this.userNetworkBtn.Location = new System.Drawing.Point(12, 241);
      this.userNetworkBtn.Name = "userNetworkBtn";
      this.userNetworkBtn.Size = new System.Drawing.Size(75, 23);
      this.userNetworkBtn.TabIndex = 24;
      this.userNetworkBtn.Text = "UsrNet";
      this.userNetworkBtn.UseVisualStyleBackColor = true;
      this.userNetworkBtn.Click += new System.EventHandler(this.userNetworkBtn_Click);
      // 
      // radioCircular
      // 
      this.radioCircular.AutoSize = true;
      this.radioCircular.Location = new System.Drawing.Point(16, 323);
      this.radioCircular.Name = "radioCircular";
      this.radioCircular.Size = new System.Drawing.Size(67, 16);
      this.radioCircular.TabIndex = 25;
      this.radioCircular.TabStop = true;
      this.radioCircular.Text = "Circular";
      this.radioCircular.UseVisualStyleBackColor = true;
      this.radioCircular.CheckedChanged += new System.EventHandler(this.radioCircular_CheckedChanged);
      // 
      // radioHierachical
      // 
      this.radioHierachical.AutoSize = true;
      this.radioHierachical.Location = new System.Drawing.Point(16, 346);
      this.radioHierachical.Name = "radioHierachical";
      this.radioHierachical.Size = new System.Drawing.Size(86, 16);
      this.radioHierachical.TabIndex = 26;
      this.radioHierachical.TabStop = true;
      this.radioHierachical.Text = "Hierachical";
      this.radioHierachical.UseVisualStyleBackColor = true;
      this.radioHierachical.CheckedChanged += new System.EventHandler(this.radioHierachical_CheckedChanged);
      // 
      // radioOrthogonal
      // 
      this.radioOrthogonal.AutoSize = true;
      this.radioOrthogonal.Location = new System.Drawing.Point(108, 346);
      this.radioOrthogonal.Name = "radioOrthogonal";
      this.radioOrthogonal.Size = new System.Drawing.Size(84, 16);
      this.radioOrthogonal.TabIndex = 27;
      this.radioOrthogonal.TabStop = true;
      this.radioOrthogonal.Text = "Orthogonal";
      this.radioOrthogonal.UseVisualStyleBackColor = true;
      this.radioOrthogonal.CheckedChanged += new System.EventHandler(this.radioOrthogonal_CheckedChanged);
      // 
      // radioTree
      // 
      this.radioTree.AutoSize = true;
      this.radioTree.Location = new System.Drawing.Point(16, 368);
      this.radioTree.Name = "radioTree";
      this.radioTree.Size = new System.Drawing.Size(49, 16);
      this.radioTree.TabIndex = 28;
      this.radioTree.TabStop = true;
      this.radioTree.Text = "Tree";
      this.radioTree.UseVisualStyleBackColor = true;
      this.radioTree.CheckedChanged += new System.EventHandler(this.radioTree_CheckedChanged);
      // 
      // labelGraphLayoutSelector
      // 
      this.labelGraphLayoutSelector.AutoSize = true;
      this.labelGraphLayoutSelector.BackColor = System.Drawing.Color.Transparent;
      this.labelGraphLayoutSelector.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelGraphLayoutSelector.ForeColor = System.Drawing.Color.Gray;
      this.labelGraphLayoutSelector.Location = new System.Drawing.Point(12, 298);
      this.labelGraphLayoutSelector.Name = "labelGraphLayoutSelector";
      this.labelGraphLayoutSelector.Size = new System.Drawing.Size(188, 22);
      this.labelGraphLayoutSelector.TabIndex = 29;
      this.labelGraphLayoutSelector.Text = "Graph Layout Selector";
      // 
      // radioOrganic
      // 
      this.radioOrganic.AutoSize = true;
      this.radioOrganic.Location = new System.Drawing.Point(108, 323);
      this.radioOrganic.Name = "radioOrganic";
      this.radioOrganic.Size = new System.Drawing.Size(67, 16);
      this.radioOrganic.TabIndex = 30;
      this.radioOrganic.TabStop = true;
      this.radioOrganic.Text = "Organic";
      this.radioOrganic.UseVisualStyleBackColor = true;
      this.radioOrganic.CheckedChanged += new System.EventHandler(this.radioOrganic_CheckedChanged);
      // 
      // imgSaveBtn
      // 
      this.imgSaveBtn.Location = new System.Drawing.Point(12, 167);
      this.imgSaveBtn.Name = "imgSaveBtn";
      this.imgSaveBtn.Size = new System.Drawing.Size(75, 23);
      this.imgSaveBtn.TabIndex = 31;
      this.imgSaveBtn.Text = "Image";
      this.imgSaveBtn.UseVisualStyleBackColor = true;
      // 
      // labelLoad
      // 
      this.labelLoad.AutoSize = true;
      this.labelLoad.BackColor = System.Drawing.Color.Transparent;
      this.labelLoad.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelLoad.ForeColor = System.Drawing.Color.Gray;
      this.labelLoad.Location = new System.Drawing.Point(12, 9);
      this.labelLoad.Name = "labelLoad";
      this.labelLoad.Size = new System.Drawing.Size(91, 22);
      this.labelLoad.TabIndex = 32;
      this.labelLoad.Text = "Load Data";
      // 
      // labelFileout
      // 
      this.labelFileout.AutoSize = true;
      this.labelFileout.BackColor = System.Drawing.Color.Transparent;
      this.labelFileout.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelFileout.ForeColor = System.Drawing.Color.Gray;
      this.labelFileout.Location = new System.Drawing.Point(188, 9);
      this.labelFileout.Name = "labelFileout";
      this.labelFileout.Size = new System.Drawing.Size(101, 22);
      this.labelFileout.TabIndex = 33;
      this.labelFileout.Text = "CSV Fileout";
      // 
      // numericEdgeThreshold
      // 
      this.numericEdgeThreshold.Location = new System.Drawing.Point(93, 243);
      this.numericEdgeThreshold.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericEdgeThreshold.Name = "numericEdgeThreshold";
      this.numericEdgeThreshold.Size = new System.Drawing.Size(44, 21);
      this.numericEdgeThreshold.TabIndex = 34;
      this.numericEdgeThreshold.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.BackColor = System.Drawing.Color.Transparent;
      this.label1.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.label1.ForeColor = System.Drawing.Color.Gray;
      this.label1.Location = new System.Drawing.Point(12, 216);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(134, 22);
      this.label1.TabIndex = 35;
      this.label1.Text = "Graph Drawing";
      // 
      // labelGraphSave
      // 
      this.labelGraphSave.AutoSize = true;
      this.labelGraphSave.BackColor = System.Drawing.Color.Transparent;
      this.labelGraphSave.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelGraphSave.ForeColor = System.Drawing.Color.Gray;
      this.labelGraphSave.Location = new System.Drawing.Point(12, 142);
      this.labelGraphSave.Name = "labelGraphSave";
      this.labelGraphSave.Size = new System.Drawing.Size(101, 22);
      this.labelGraphSave.TabIndex = 37;
      this.labelGraphSave.Text = "Graph Save";
      // 
      // labelNode
      // 
      this.labelNode.AutoSize = true;
      this.labelNode.BackColor = System.Drawing.Color.Transparent;
      this.labelNode.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNode.ForeColor = System.Drawing.Color.Gray;
      this.labelNode.Location = new System.Drawing.Point(189, 31);
      this.labelNode.Name = "labelNode";
      this.labelNode.Size = new System.Drawing.Size(66, 15);
      this.labelNode.TabIndex = 38;
      this.labelNode.Text = "N : #nodes";
      // 
      // labelParticipants
      // 
      this.labelParticipants.AutoSize = true;
      this.labelParticipants.BackColor = System.Drawing.Color.Transparent;
      this.labelParticipants.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelParticipants.ForeColor = System.Drawing.Color.Gray;
      this.labelParticipants.Location = new System.Drawing.Point(189, 46);
      this.labelParticipants.Name = "labelParticipants";
      this.labelParticipants.Size = new System.Drawing.Size(97, 15);
      this.labelParticipants.TabIndex = 39;
      this.labelParticipants.Text = "P : #participants";
      // 
      // labelView
      // 
      this.labelView.AutoSize = true;
      this.labelView.BackColor = System.Drawing.Color.Transparent;
      this.labelView.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelView.ForeColor = System.Drawing.Color.Gray;
      this.labelView.Location = new System.Drawing.Point(292, 34);
      this.labelView.Name = "labelView";
      this.labelView.Size = new System.Drawing.Size(65, 15);
      this.labelView.TabIndex = 40;
      this.labelView.Text = "V : #views";
      // 
      // labelArticleNo
      // 
      this.labelArticleNo.AutoSize = true;
      this.labelArticleNo.BackColor = System.Drawing.Color.Transparent;
      this.labelArticleNo.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelArticleNo.ForeColor = System.Drawing.Color.Gray;
      this.labelArticleNo.Location = new System.Drawing.Point(292, 46);
      this.labelArticleNo.Name = "labelArticleNo";
      this.labelArticleNo.Size = new System.Drawing.Size(79, 15);
      this.labelArticleNo.TabIndex = 41;
      this.labelArticleNo.Text = "A : article no.";
      // 
      // numericNodeThreshold
      // 
      this.numericNodeThreshold.Location = new System.Drawing.Point(211, 79);
      this.numericNodeThreshold.Maximum = new decimal(new int[] {
            500,
            0,
            0,
            0});
      this.numericNodeThreshold.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericNodeThreshold.Name = "numericNodeThreshold";
      this.numericNodeThreshold.Size = new System.Drawing.Size(44, 21);
      this.numericNodeThreshold.TabIndex = 44;
      this.numericNodeThreshold.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // numericDepthThreshold
      // 
      this.numericDepthThreshold.Location = new System.Drawing.Point(313, 79);
      this.numericDepthThreshold.Minimum = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericDepthThreshold.Name = "numericDepthThreshold";
      this.numericDepthThreshold.Size = new System.Drawing.Size(44, 21);
      this.numericDepthThreshold.TabIndex = 45;
      this.numericDepthThreshold.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      // 
      // labelNodeThrsDiff
      // 
      this.labelNodeThrsDiff.AutoSize = true;
      this.labelNodeThrsDiff.BackColor = System.Drawing.Color.Transparent;
      this.labelNodeThrsDiff.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNodeThrsDiff.ForeColor = System.Drawing.Color.Gray;
      this.labelNodeThrsDiff.Location = new System.Drawing.Point(189, 79);
      this.labelNodeThrsDiff.Name = "labelNodeThrsDiff";
      this.labelNodeThrsDiff.Size = new System.Drawing.Size(22, 15);
      this.labelNodeThrsDiff.TabIndex = 46;
      this.labelNodeThrsDiff.Text = "#n";
      // 
      // labelDepthThrs
      // 
      this.labelDepthThrs.AutoSize = true;
      this.labelDepthThrs.BackColor = System.Drawing.Color.Transparent;
      this.labelDepthThrs.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDepthThrs.ForeColor = System.Drawing.Color.Gray;
      this.labelDepthThrs.Location = new System.Drawing.Point(275, 79);
      this.labelDepthThrs.Name = "labelDepthThrs";
      this.labelDepthThrs.Size = new System.Drawing.Size(38, 15);
      this.labelDepthThrs.TabIndex = 47;
      this.labelDepthThrs.Text = "depth";
      // 
      // labelPortionThreshold
      // 
      this.labelPortionThreshold.AutoSize = true;
      this.labelPortionThreshold.BackColor = System.Drawing.Color.Transparent;
      this.labelPortionThreshold.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelPortionThreshold.ForeColor = System.Drawing.Color.Gray;
      this.labelPortionThreshold.Location = new System.Drawing.Point(363, 79);
      this.labelPortionThreshold.Name = "labelPortionThreshold";
      this.labelPortionThreshold.Size = new System.Drawing.Size(19, 15);
      this.labelPortionThreshold.TabIndex = 52;
      this.labelPortionThreshold.Text = "%";
      // 
      // numericRatio
      // 
      this.numericRatio.Increment = new decimal(new int[] {
            5,
            0,
            0,
            65536});
      this.numericRatio.Location = new System.Drawing.Point(385, 79);
      this.numericRatio.Minimum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericRatio.Name = "numericRatio";
      this.numericRatio.Size = new System.Drawing.Size(44, 21);
      this.numericRatio.TabIndex = 51;
      this.numericRatio.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      // 
      // labelWriterReply
      // 
      this.labelWriterReply.AutoSize = true;
      this.labelWriterReply.BackColor = System.Drawing.Color.Transparent;
      this.labelWriterReply.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelWriterReply.ForeColor = System.Drawing.Color.Gray;
      this.labelWriterReply.Location = new System.Drawing.Point(189, 61);
      this.labelWriterReply.Name = "labelWriterReply";
      this.labelWriterReply.Size = new System.Drawing.Size(86, 15);
      this.labelWriterReply.TabIndex = 54;
      this.labelWriterReply.Text = "W : #WriterRe";
      // 
      // btnMatView
      // 
      this.btnMatView.Location = new System.Drawing.Point(192, 167);
      this.btnMatView.Name = "btnMatView";
      this.btnMatView.Size = new System.Drawing.Size(75, 23);
      this.btnMatView.TabIndex = 57;
      this.btnMatView.Text = "for view";
      this.btnMatView.UseVisualStyleBackColor = true;
      this.btnMatView.Click += new System.EventHandler(this.btnMatView_Click);
      // 
      // labelArticleMat
      // 
      this.labelArticleMat.AutoSize = true;
      this.labelArticleMat.BackColor = System.Drawing.Color.Transparent;
      this.labelArticleMat.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelArticleMat.ForeColor = System.Drawing.Color.Gray;
      this.labelArticleMat.Location = new System.Drawing.Point(192, 142);
      this.labelArticleMat.Name = "labelArticleMat";
      this.labelArticleMat.Size = new System.Drawing.Size(98, 22);
      this.labelArticleMat.TabIndex = 58;
      this.labelArticleMat.Text = "Article Mat";
      // 
      // btnMatReply
      // 
      this.btnMatReply.Location = new System.Drawing.Point(273, 167);
      this.btnMatReply.Name = "btnMatReply";
      this.btnMatReply.Size = new System.Drawing.Size(75, 23);
      this.btnMatReply.TabIndex = 59;
      this.btnMatReply.Text = "for reply";
      this.btnMatReply.UseVisualStyleBackColor = true;
      // 
      // articleFileoutBtn
      // 
      this.articleFileoutBtn.Location = new System.Drawing.Point(192, 106);
      this.articleFileoutBtn.Name = "articleFileoutBtn";
      this.articleFileoutBtn.Size = new System.Drawing.Size(75, 23);
      this.articleFileoutBtn.TabIndex = 61;
      this.articleFileoutBtn.Text = "Article";
      this.articleFileoutBtn.UseVisualStyleBackColor = true;
      this.articleFileoutBtn.Click += new System.EventHandler(this.fileoutBtn_Click);
      // 
      // saveArticleTempBtn
      // 
      this.saveArticleTempBtn.Location = new System.Drawing.Point(374, 296);
      this.saveArticleTempBtn.Name = "saveArticleTempBtn";
      this.saveArticleTempBtn.Size = new System.Drawing.Size(75, 23);
      this.saveArticleTempBtn.TabIndex = 63;
      this.saveArticleTempBtn.Text = "temp.";
      this.saveArticleTempBtn.UseVisualStyleBackColor = true;
      this.saveArticleTempBtn.Click += new System.EventHandler(this.saveArticleTempBtn_Click);
      // 
      // userFileoutBtn
      // 
      this.userFileoutBtn.Location = new System.Drawing.Point(273, 106);
      this.userFileoutBtn.Name = "userFileoutBtn";
      this.userFileoutBtn.Size = new System.Drawing.Size(75, 23);
      this.userFileoutBtn.TabIndex = 64;
      this.userFileoutBtn.Text = "User";
      this.userFileoutBtn.UseVisualStyleBackColor = true;
      this.userFileoutBtn.Click += new System.EventHandler(this.userFileoutBtn_Click);
      // 
      // labelViews
      // 
      this.labelViews.AutoSize = true;
      this.labelViews.BackColor = System.Drawing.Color.Transparent;
      this.labelViews.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelViews.ForeColor = System.Drawing.Color.Gray;
      this.labelViews.Location = new System.Drawing.Point(12, 444);
      this.labelViews.Name = "labelViews";
      this.labelViews.Size = new System.Drawing.Size(58, 22);
      this.labelViews.TabIndex = 65;
      this.labelViews.Text = "Views";
      // 
      // labelViewInfo
      // 
      this.labelViewInfo.AutoSize = true;
      this.labelViewInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelViewInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelViewInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelViewInfo.Location = new System.Drawing.Point(150, 444);
      this.labelViewInfo.Name = "labelViewInfo";
      this.labelViewInfo.Size = new System.Drawing.Size(99, 22);
      this.labelViewInfo.TabIndex = 66;
      this.labelViewInfo.Text = "view count";
      // 
      // labelNumReplyInfo
      // 
      this.labelNumReplyInfo.AutoSize = true;
      this.labelNumReplyInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelNumReplyInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumReplyInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelNumReplyInfo.Location = new System.Drawing.Point(151, 466);
      this.labelNumReplyInfo.Name = "labelNumReplyInfo";
      this.labelNumReplyInfo.Size = new System.Drawing.Size(77, 22);
      this.labelNumReplyInfo.TabIndex = 68;
      this.labelNumReplyInfo.Text = "#replies";
      // 
      // labelNumReply
      // 
      this.labelNumReply.AutoSize = true;
      this.labelNumReply.BackColor = System.Drawing.Color.Transparent;
      this.labelNumReply.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumReply.ForeColor = System.Drawing.Color.Gray;
      this.labelNumReply.Location = new System.Drawing.Point(13, 466);
      this.labelNumReply.Name = "labelNumReply";
      this.labelNumReply.Size = new System.Drawing.Size(81, 22);
      this.labelNumReply.TabIndex = 67;
      this.labelNumReply.Text = "#Replies";
      // 
      // labelArticleInfo
      // 
      this.labelArticleInfo.AutoSize = true;
      this.labelArticleInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelArticleInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelArticleInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelArticleInfo.Location = new System.Drawing.Point(151, 400);
      this.labelArticleInfo.Name = "labelArticleInfo";
      this.labelArticleInfo.Size = new System.Drawing.Size(129, 22);
      this.labelArticleInfo.TabIndex = 70;
      this.labelArticleInfo.Text = "article number";
      // 
      // labelArticle
      // 
      this.labelArticle.AutoSize = true;
      this.labelArticle.BackColor = System.Drawing.Color.Transparent;
      this.labelArticle.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelArticle.ForeColor = System.Drawing.Color.Gray;
      this.labelArticle.Location = new System.Drawing.Point(13, 400);
      this.labelArticle.Name = "labelArticle";
      this.labelArticle.Size = new System.Drawing.Size(94, 22);
      this.labelArticle.TabIndex = 69;
      this.labelArticle.Text = "Article No.";
      // 
      // chartTemperature
      // 
      this.chartTemperature.ForeColor = System.Drawing.SystemColors.Control;
      this.chartTemperature.Location = new System.Drawing.Point(455, 504);
      this.chartTemperature.Name = "chartTemperature";
      this.chartTemperature.PropBag = resources.GetString("chartTemperature.PropBag");
      this.chartTemperature.Size = new System.Drawing.Size(1149, 456);
      this.chartTemperature.TabIndex = 71;
      // 
      // numericSymbolSize
      // 
      this.numericSymbolSize.Location = new System.Drawing.Point(341, 244);
      this.numericSymbolSize.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericSymbolSize.Name = "numericSymbolSize";
      this.numericSymbolSize.Size = new System.Drawing.Size(33, 21);
      this.numericSymbolSize.TabIndex = 72;
      this.numericSymbolSize.Value = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericSymbolSize.ValueChanged += new System.EventHandler(this.numericSymbolSize_ValueChanged);
      // 
      // numericLineSize
      // 
      this.numericLineSize.Location = new System.Drawing.Point(409, 244);
      this.numericLineSize.Maximum = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericLineSize.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            0});
      this.numericLineSize.Name = "numericLineSize";
      this.numericLineSize.Size = new System.Drawing.Size(33, 21);
      this.numericLineSize.TabIndex = 73;
      this.numericLineSize.Value = new decimal(new int[] {
            2,
            0,
            0,
            0});
      this.numericLineSize.ValueChanged += new System.EventHandler(this.numericLineSize_ValueChanged);
      // 
      // labelLine
      // 
      this.labelLine.AutoSize = true;
      this.labelLine.BackColor = System.Drawing.Color.Transparent;
      this.labelLine.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelLine.ForeColor = System.Drawing.Color.Gray;
      this.labelLine.Location = new System.Drawing.Point(378, 246);
      this.labelLine.Name = "labelLine";
      this.labelLine.Size = new System.Drawing.Size(28, 15);
      this.labelLine.TabIndex = 74;
      this.labelLine.Text = "line";
      // 
      // labelSymbol
      // 
      this.labelSymbol.AutoSize = true;
      this.labelSymbol.BackColor = System.Drawing.Color.Transparent;
      this.labelSymbol.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelSymbol.ForeColor = System.Drawing.Color.Gray;
      this.labelSymbol.Location = new System.Drawing.Point(292, 246);
      this.labelSymbol.Name = "labelSymbol";
      this.labelSymbol.Size = new System.Drawing.Size(49, 15);
      this.labelSymbol.TabIndex = 75;
      this.labelSymbol.Text = "symbol";
      // 
      // numericK
      // 
      this.numericK.Location = new System.Drawing.Point(456, 518);
      this.numericK.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
      this.numericK.Name = "numericK";
      this.numericK.Size = new System.Drawing.Size(44, 21);
      this.numericK.TabIndex = 76;
      this.numericK.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
      this.numericK.ValueChanged += new System.EventHandler(this.numericK_ValueChanged);
      // 
      // numericC0
      // 
      this.numericC0.Location = new System.Drawing.Point(506, 518);
      this.numericC0.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
      this.numericC0.Name = "numericC0";
      this.numericC0.Size = new System.Drawing.Size(44, 21);
      this.numericC0.TabIndex = 77;
      this.numericC0.Value = new decimal(new int[] {
            1000,
            0,
            0,
            0});
      this.numericC0.ValueChanged += new System.EventHandler(this.numericC0_ValueChanged);
      // 
      // numericC1
      // 
      this.numericC1.Location = new System.Drawing.Point(556, 518);
      this.numericC1.Maximum = new decimal(new int[] {
            999999,
            0,
            0,
            0});
      this.numericC1.Name = "numericC1";
      this.numericC1.Size = new System.Drawing.Size(44, 21);
      this.numericC1.TabIndex = 78;
      this.numericC1.Value = new decimal(new int[] {
            10,
            0,
            0,
            0});
      this.numericC1.ValueChanged += new System.EventHandler(this.numericC1_ValueChanged);
      // 
      // labelK
      // 
      this.labelK.AutoSize = true;
      this.labelK.BackColor = System.Drawing.Color.Transparent;
      this.labelK.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelK.ForeColor = System.Drawing.Color.Gray;
      this.labelK.Location = new System.Drawing.Point(466, 499);
      this.labelK.Name = "labelK";
      this.labelK.Size = new System.Drawing.Size(14, 15);
      this.labelK.TabIndex = 79;
      this.labelK.Text = "k";
      // 
      // labelC0
      // 
      this.labelC0.AutoSize = true;
      this.labelC0.BackColor = System.Drawing.Color.Transparent;
      this.labelC0.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelC0.ForeColor = System.Drawing.Color.Gray;
      this.labelC0.Location = new System.Drawing.Point(516, 500);
      this.labelC0.Name = "labelC0";
      this.labelC0.Size = new System.Drawing.Size(20, 15);
      this.labelC0.TabIndex = 80;
      this.labelC0.Text = "c0";
      // 
      // labelC1
      // 
      this.labelC1.AutoSize = true;
      this.labelC1.BackColor = System.Drawing.Color.Transparent;
      this.labelC1.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelC1.ForeColor = System.Drawing.Color.Gray;
      this.labelC1.Location = new System.Drawing.Point(562, 500);
      this.labelC1.Name = "labelC1";
      this.labelC1.Size = new System.Drawing.Size(20, 15);
      this.labelC1.TabIndex = 81;
      this.labelC1.Text = "c1";
      // 
      // labelDecRat
      // 
      this.labelDecRat.AutoSize = true;
      this.labelDecRat.BackColor = System.Drawing.Color.Transparent;
      this.labelDecRat.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDecRat.ForeColor = System.Drawing.Color.Gray;
      this.labelDecRat.Location = new System.Drawing.Point(599, 499);
      this.labelDecRat.Name = "labelDecRat";
      this.labelDecRat.Size = new System.Drawing.Size(53, 15);
      this.labelDecRat.TabIndex = 83;
      this.labelDecRat.Text = "dec. rate";
      // 
      // numericDecRat
      // 
      this.numericDecRat.DecimalPlaces = 2;
      this.numericDecRat.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.numericDecRat.Location = new System.Drawing.Point(606, 518);
      this.numericDecRat.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
      this.numericDecRat.Name = "numericDecRat";
      this.numericDecRat.Size = new System.Drawing.Size(44, 21);
      this.numericDecRat.TabIndex = 82;
      this.numericDecRat.Value = new decimal(new int[] {
            99,
            0,
            0,
            131072});
      this.numericDecRat.ValueChanged += new System.EventHandler(this.numericDecRat_ValueChanged);
      // 
      // labelDeadDur
      // 
      this.labelDeadDur.AutoSize = true;
      this.labelDeadDur.BackColor = System.Drawing.Color.Transparent;
      this.labelDeadDur.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelDeadDur.ForeColor = System.Drawing.Color.Gray;
      this.labelDeadDur.Location = new System.Drawing.Point(649, 499);
      this.labelDeadDur.Name = "labelDeadDur";
      this.labelDeadDur.Size = new System.Drawing.Size(56, 15);
      this.labelDeadDur.TabIndex = 85;
      this.labelDeadDur.Text = "DeadDur";
      // 
      // numericDeadDuration
      // 
      this.numericDeadDuration.Location = new System.Drawing.Point(656, 518);
      this.numericDeadDuration.Maximum = new decimal(new int[] {
            24,
            0,
            0,
            0});
      this.numericDeadDuration.Name = "numericDeadDuration";
      this.numericDeadDuration.Size = new System.Drawing.Size(44, 21);
      this.numericDeadDuration.TabIndex = 84;
      this.numericDeadDuration.Value = new decimal(new int[] {
            24,
            0,
            0,
            0});
      this.numericDeadDuration.ValueChanged += new System.EventHandler(this.numericDeadDuration_ValueChanged);
      // 
      // labelNumLeaf
      // 
      this.labelNumLeaf.AutoSize = true;
      this.labelNumLeaf.BackColor = System.Drawing.Color.Transparent;
      this.labelNumLeaf.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumLeaf.ForeColor = System.Drawing.Color.Gray;
      this.labelNumLeaf.Location = new System.Drawing.Point(13, 615);
      this.labelNumLeaf.Name = "labelNumLeaf";
      this.labelNumLeaf.Size = new System.Drawing.Size(56, 22);
      this.labelNumLeaf.TabIndex = 87;
      this.labelNumLeaf.Text = "#Leaf";
      // 
      // labelNumLeafInfo
      // 
      this.labelNumLeafInfo.AutoSize = true;
      this.labelNumLeafInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelNumLeafInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelNumLeafInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelNumLeafInfo.Location = new System.Drawing.Point(151, 615);
      this.labelNumLeafInfo.Name = "labelNumLeafInfo";
      this.labelNumLeafInfo.Size = new System.Drawing.Size(104, 22);
      this.labelNumLeafInfo.TabIndex = 86;
      this.labelNumLeafInfo.Text = "#leaf nodes";
      // 
      // labelLifetime
      // 
      this.labelLifetime.AutoSize = true;
      this.labelLifetime.BackColor = System.Drawing.Color.Transparent;
      this.labelLifetime.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelLifetime.ForeColor = System.Drawing.Color.Gray;
      this.labelLifetime.Location = new System.Drawing.Point(13, 656);
      this.labelLifetime.Name = "labelLifetime";
      this.labelLifetime.Size = new System.Drawing.Size(76, 22);
      this.labelLifetime.TabIndex = 89;
      this.labelLifetime.Text = "Lifetime";
      // 
      // labelLifetimeInfo
      // 
      this.labelLifetimeInfo.AutoSize = true;
      this.labelLifetimeInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelLifetimeInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelLifetimeInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelLifetimeInfo.Location = new System.Drawing.Point(151, 656);
      this.labelLifetimeInfo.Name = "labelLifetimeInfo";
      this.labelLifetimeInfo.Size = new System.Drawing.Size(71, 22);
      this.labelLifetimeInfo.TabIndex = 88;
      this.labelLifetimeInfo.Text = "lifetime";
      // 
      // labelTempsum
      // 
      this.labelTempsum.AutoSize = true;
      this.labelTempsum.BackColor = System.Drawing.Color.Transparent;
      this.labelTempsum.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempsum.ForeColor = System.Drawing.Color.Gray;
      this.labelTempsum.Location = new System.Drawing.Point(13, 678);
      this.labelTempsum.Name = "labelTempsum";
      this.labelTempsum.Size = new System.Drawing.Size(91, 22);
      this.labelTempsum.TabIndex = 91;
      this.labelTempsum.Text = "TempSum";
      // 
      // labelTempsumInfo
      // 
      this.labelTempsumInfo.AutoSize = true;
      this.labelTempsumInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelTempsumInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempsumInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelTempsumInfo.Location = new System.Drawing.Point(151, 678);
      this.labelTempsumInfo.Name = "labelTempsumInfo";
      this.labelTempsumInfo.Size = new System.Drawing.Size(171, 22);
      this.labelTempsumInfo.TabIndex = 90;
      this.labelTempsumInfo.Text = "sum of temperature";
      // 
      // labelIPL
      // 
      this.labelIPL.AutoSize = true;
      this.labelIPL.BackColor = System.Drawing.Color.Transparent;
      this.labelIPL.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelIPL.ForeColor = System.Drawing.Color.Gray;
      this.labelIPL.Location = new System.Drawing.Point(13, 700);
      this.labelIPL.Name = "labelIPL";
      this.labelIPL.Size = new System.Drawing.Size(37, 22);
      this.labelIPL.TabIndex = 93;
      this.labelIPL.Text = "IPL";
      // 
      // labelIPLInfo
      // 
      this.labelIPLInfo.AutoSize = true;
      this.labelIPLInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelIPLInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelIPLInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelIPLInfo.Location = new System.Drawing.Point(151, 700);
      this.labelIPLInfo.Name = "labelIPLInfo";
      this.labelIPLInfo.Size = new System.Drawing.Size(227, 22);
      this.labelIPLInfo.TabIndex = 92;
      this.labelIPLInfo.Text = "sum of internal path length";
      // 
      // labelXPL
      // 
      this.labelXPL.AutoSize = true;
      this.labelXPL.BackColor = System.Drawing.Color.Transparent;
      this.labelXPL.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelXPL.ForeColor = System.Drawing.Color.Gray;
      this.labelXPL.Location = new System.Drawing.Point(12, 722);
      this.labelXPL.Name = "labelXPL";
      this.labelXPL.Size = new System.Drawing.Size(42, 22);
      this.labelXPL.TabIndex = 95;
      this.labelXPL.Text = "XPL";
      // 
      // labelXPLInfo
      // 
      this.labelXPLInfo.AutoSize = true;
      this.labelXPLInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelXPLInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelXPLInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelXPLInfo.Location = new System.Drawing.Point(150, 722);
      this.labelXPLInfo.Name = "labelXPLInfo";
      this.labelXPLInfo.Size = new System.Drawing.Size(229, 22);
      this.labelXPLInfo.TabIndex = 94;
      this.labelXPLInfo.Text = "sum of external path length";
      // 
      // labelTempPeak
      // 
      this.labelTempPeak.AutoSize = true;
      this.labelTempPeak.BackColor = System.Drawing.Color.Transparent;
      this.labelTempPeak.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempPeak.ForeColor = System.Drawing.Color.Gray;
      this.labelTempPeak.Location = new System.Drawing.Point(13, 744);
      this.labelTempPeak.Name = "labelTempPeak";
      this.labelTempPeak.Size = new System.Drawing.Size(94, 22);
      this.labelTempPeak.TabIndex = 97;
      this.labelTempPeak.Text = "TempPeak";
      // 
      // labelTempPeakInfo
      // 
      this.labelTempPeakInfo.AutoSize = true;
      this.labelTempPeakInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelTempPeakInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempPeakInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelTempPeakInfo.Location = new System.Drawing.Point(151, 744);
      this.labelTempPeakInfo.Name = "labelTempPeakInfo";
      this.labelTempPeakInfo.Size = new System.Drawing.Size(223, 22);
      this.labelTempPeakInfo.TabIndex = 96;
      this.labelTempPeakInfo.Text = "Peak value of temperature";
      // 
      // btnTempParamDefault
      // 
      this.btnTempParamDefault.Location = new System.Drawing.Point(705, 516);
      this.btnTempParamDefault.Name = "btnTempParamDefault";
      this.btnTempParamDefault.Size = new System.Drawing.Size(52, 23);
      this.btnTempParamDefault.TabIndex = 98;
      this.btnTempParamDefault.Text = "default";
      this.btnTempParamDefault.UseVisualStyleBackColor = true;
      this.btnTempParamDefault.Click += new System.EventHandler(this.btnTempParamDefault_Click);
      // 
      // labelTempPeakTime
      // 
      this.labelTempPeakTime.AutoSize = true;
      this.labelTempPeakTime.BackColor = System.Drawing.Color.Transparent;
      this.labelTempPeakTime.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempPeakTime.ForeColor = System.Drawing.Color.Gray;
      this.labelTempPeakTime.Location = new System.Drawing.Point(13, 766);
      this.labelTempPeakTime.Name = "labelTempPeakTime";
      this.labelTempPeakTime.Size = new System.Drawing.Size(135, 22);
      this.labelTempPeakTime.TabIndex = 100;
      this.labelTempPeakTime.Text = "TempPeakTime";
      // 
      // labelTempPeakTimeInfo
      // 
      this.labelTempPeakTimeInfo.AutoSize = true;
      this.labelTempPeakTimeInfo.BackColor = System.Drawing.SystemColors.Control;
      this.labelTempPeakTimeInfo.Font = new System.Drawing.Font("Cambria", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelTempPeakTimeInfo.ForeColor = System.Drawing.Color.Blue;
      this.labelTempPeakTimeInfo.Location = new System.Drawing.Point(151, 766);
      this.labelTempPeakTimeInfo.Name = "labelTempPeakTimeInfo";
      this.labelTempPeakTimeInfo.Size = new System.Drawing.Size(215, 22);
      this.labelTempPeakTimeInfo.TabIndex = 99;
      this.labelTempPeakTimeInfo.Text = "Peak time of temperature";
      // 
      // btnSubtree
      // 
      this.btnSubtree.Location = new System.Drawing.Point(12, 61);
      this.btnSubtree.Name = "btnSubtree";
      this.btnSubtree.Size = new System.Drawing.Size(75, 23);
      this.btnSubtree.TabIndex = 101;
      this.btnSubtree.Text = "subtree";
      this.btnSubtree.UseVisualStyleBackColor = true;
      this.btnSubtree.Click += new System.EventHandler(this.btnSubtree_Click);
      // 
      // labelFocusThrs
      // 
      this.labelFocusThrs.AutoSize = true;
      this.labelFocusThrs.BackColor = System.Drawing.Color.Transparent;
      this.labelFocusThrs.Font = new System.Drawing.Font("Cambria", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
      this.labelFocusThrs.ForeColor = System.Drawing.Color.Gray;
      this.labelFocusThrs.Location = new System.Drawing.Point(193, 246);
      this.labelFocusThrs.Name = "labelFocusThrs";
      this.labelFocusThrs.Size = new System.Drawing.Size(52, 15);
      this.labelFocusThrs.TabIndex = 103;
      this.labelFocusThrs.Text = "Focused";
      // 
      // numericFocused
      // 
      this.numericFocused.DecimalPlaces = 2;
      this.numericFocused.Increment = new decimal(new int[] {
            1,
            0,
            0,
            131072});
      this.numericFocused.Location = new System.Drawing.Point(246, 244);
      this.numericFocused.Maximum = new decimal(new int[] {
            99,
            0,
            0,
            131072});
      this.numericFocused.Name = "numericFocused";
      this.numericFocused.Size = new System.Drawing.Size(44, 21);
      this.numericFocused.TabIndex = 104;
      this.numericFocused.Value = new decimal(new int[] {
            40,
            0,
            0,
            131072});
      this.numericFocused.ValueChanged += new System.EventHandler(this.numericFocused_ValueChanged);
      // 
      // Form1
      // 
      this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.ClientSize = new System.Drawing.Size(1616, 972);
      this.Controls.Add(this.numericFocused);
      this.Controls.Add(this.labelFocusThrs);
      this.Controls.Add(this.btnSubtree);
      this.Controls.Add(this.labelTempPeakTime);
      this.Controls.Add(this.labelTempPeakTimeInfo);
      this.Controls.Add(this.btnTempParamDefault);
      this.Controls.Add(this.labelTempPeak);
      this.Controls.Add(this.labelTempPeakInfo);
      this.Controls.Add(this.labelXPL);
      this.Controls.Add(this.labelXPLInfo);
      this.Controls.Add(this.labelIPL);
      this.Controls.Add(this.labelIPLInfo);
      this.Controls.Add(this.labelTempsum);
      this.Controls.Add(this.labelTempsumInfo);
      this.Controls.Add(this.labelLifetime);
      this.Controls.Add(this.labelLifetimeInfo);
      this.Controls.Add(this.labelNumLeaf);
      this.Controls.Add(this.labelNumLeafInfo);
      this.Controls.Add(this.labelDeadDur);
      this.Controls.Add(this.numericDeadDuration);
      this.Controls.Add(this.labelDecRat);
      this.Controls.Add(this.numericDecRat);
      this.Controls.Add(this.labelC1);
      this.Controls.Add(this.labelC0);
      this.Controls.Add(this.labelK);
      this.Controls.Add(this.numericC1);
      this.Controls.Add(this.numericC0);
      this.Controls.Add(this.numericK);
      this.Controls.Add(this.labelSymbol);
      this.Controls.Add(this.labelLine);
      this.Controls.Add(this.numericLineSize);
      this.Controls.Add(this.numericSymbolSize);
      this.Controls.Add(this.chartTemperature);
      this.Controls.Add(this.labelArticleInfo);
      this.Controls.Add(this.labelArticle);
      this.Controls.Add(this.labelNumReplyInfo);
      this.Controls.Add(this.labelNumReply);
      this.Controls.Add(this.labelViewInfo);
      this.Controls.Add(this.labelViews);
      this.Controls.Add(this.userFileoutBtn);
      this.Controls.Add(this.saveArticleTempBtn);
      this.Controls.Add(this.articleFileoutBtn);
      this.Controls.Add(this.btnMatReply);
      this.Controls.Add(this.labelArticleMat);
      this.Controls.Add(this.btnMatView);
      this.Controls.Add(this.labelWriterReply);
      this.Controls.Add(this.labelPortionThreshold);
      this.Controls.Add(this.numericRatio);
      this.Controls.Add(this.labelDepthThrs);
      this.Controls.Add(this.labelNodeThrsDiff);
      this.Controls.Add(this.numericDepthThreshold);
      this.Controls.Add(this.numericNodeThreshold);
      this.Controls.Add(this.labelArticleNo);
      this.Controls.Add(this.labelView);
      this.Controls.Add(this.labelParticipants);
      this.Controls.Add(this.labelNode);
      this.Controls.Add(this.labelGraphSave);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.numericEdgeThreshold);
      this.Controls.Add(this.labelFileout);
      this.Controls.Add(this.labelLoad);
      this.Controls.Add(this.imgSaveBtn);
      this.Controls.Add(this.radioOrganic);
      this.Controls.Add(this.labelGraphLayoutSelector);
      this.Controls.Add(this.radioTree);
      this.Controls.Add(this.radioOrthogonal);
      this.Controls.Add(this.radioHierachical);
      this.Controls.Add(this.radioCircular);
      this.Controls.Add(this.userNetworkBtn);
      this.Controls.Add(this.labelDepthOne);
      this.Controls.Add(this.labelDepthOneInfo);
      this.Controls.Add(this.labelMDegree);
      this.Controls.Add(this.labelMDegreeInfo);
      this.Controls.Add(this.labelRelatedUsers);
      this.Controls.Add(this.labelRelatedUsersInfo);
      this.Controls.Add(this.labelDepth);
      this.Controls.Add(this.labelDepthInfo);
      this.Controls.Add(this.labelTopReplier);
      this.Controls.Add(this.labelTopReplierInfo);
      this.Controls.Add(this.labelAvgDepth);
      this.Controls.Add(this.labelAvgDepthInfo);
      this.Controls.Add(this.labelNumPart);
      this.Controls.Add(this.labelWriterInfo);
      this.Controls.Add(this.labelNumPartInfo);
      this.Controls.Add(this.labelWriter);
      this.Controls.Add(this.logText);
      this.Controls.Add(this.graphControl1);
      this.Controls.Add(this.articleListCb);
      this.Controls.Add(this.loadArticleBtn);
      this.Controls.Add(this.loadUserBtn);
      this.Name = "Form1";
      this.Text = "Form1";
      ((System.ComponentModel.ISupportInitialize)(this.numericEdgeThreshold)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericNodeThreshold)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDepthThreshold)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericRatio)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.chartTemperature)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericSymbolSize)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericLineSize)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericK)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericC0)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericC1)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDecRat)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericDeadDuration)).EndInit();
      ((System.ComponentModel.ISupportInitialize)(this.numericFocused)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.Button loadUserBtn;
    private System.Windows.Forms.Button loadArticleBtn;
    private System.Windows.Forms.ComboBox articleListCb;
    private yWorks.yFiles.UI.GraphControl graphControl1;
    private System.Windows.Forms.TextBox logText;
    private System.Windows.Forms.Label labelWriter;
    private System.Windows.Forms.Label labelNumPartInfo;
    private System.Windows.Forms.Label labelWriterInfo;
    private System.Windows.Forms.Label labelNumPart;
    private System.Windows.Forms.Label labelAvgDepth;
    private System.Windows.Forms.Label labelAvgDepthInfo;
    private System.Windows.Forms.Label labelTopReplier;
    private System.Windows.Forms.Label labelTopReplierInfo;
    private System.Windows.Forms.Label labelDepth;
    private System.Windows.Forms.Label labelDepthInfo;
    private System.Windows.Forms.Label labelRelatedUsers;
    private System.Windows.Forms.Label labelRelatedUsersInfo;
    private System.Windows.Forms.Label labelMDegree;
    private System.Windows.Forms.Label labelMDegreeInfo;
    private System.Windows.Forms.Label labelDepthOne;
    private System.Windows.Forms.Label labelDepthOneInfo;
    private System.Windows.Forms.Button userNetworkBtn;
    private System.Windows.Forms.RadioButton radioCircular;
    private System.Windows.Forms.RadioButton radioHierachical;
    private System.Windows.Forms.RadioButton radioOrthogonal;
    private System.Windows.Forms.RadioButton radioTree;
    private System.Windows.Forms.Label labelGraphLayoutSelector;
    private System.Windows.Forms.RadioButton radioOrganic;
    private System.Windows.Forms.Button imgSaveBtn;
    private System.Windows.Forms.Label labelLoad;
    private System.Windows.Forms.Label labelFileout;
    private System.Windows.Forms.NumericUpDown numericEdgeThreshold;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Label labelGraphSave;
    private System.Windows.Forms.Label labelNode;
    private System.Windows.Forms.Label labelParticipants;
    private System.Windows.Forms.Label labelView;
    private System.Windows.Forms.Label labelArticleNo;
    private System.Windows.Forms.NumericUpDown numericNodeThreshold;
    private System.Windows.Forms.NumericUpDown numericDepthThreshold;
    private System.Windows.Forms.Label labelNodeThrsDiff;
    private System.Windows.Forms.Label labelDepthThrs;
    private System.Windows.Forms.Label labelPortionThreshold;
    private System.Windows.Forms.NumericUpDown numericRatio;
    private System.Windows.Forms.Label labelWriterReply;
    private System.Windows.Forms.Button btnMatView;
    private System.Windows.Forms.Label labelArticleMat;
    private System.Windows.Forms.Button btnMatReply;
    private System.Windows.Forms.Button articleFileoutBtn;
    private System.Windows.Forms.Button saveArticleTempBtn;
    private System.Windows.Forms.Button userFileoutBtn;
    private System.Windows.Forms.Label labelViews;
    private System.Windows.Forms.Label labelViewInfo;
    private System.Windows.Forms.Label labelNumReplyInfo;
    private System.Windows.Forms.Label labelNumReply;
    private System.Windows.Forms.Label labelArticleInfo;
    private System.Windows.Forms.Label labelArticle;
    private C1.Win.C1Chart.C1Chart chartTemperature;
    private System.Windows.Forms.NumericUpDown numericSymbolSize;
    private System.Windows.Forms.NumericUpDown numericLineSize;
    private System.Windows.Forms.Label labelLine;
    private System.Windows.Forms.Label labelSymbol;
    private System.Windows.Forms.NumericUpDown numericK;
    private System.Windows.Forms.NumericUpDown numericC0;
    private System.Windows.Forms.NumericUpDown numericC1;
    private System.Windows.Forms.Label labelK;
    private System.Windows.Forms.Label labelC0;
    private System.Windows.Forms.Label labelC1;
    private System.Windows.Forms.Label labelDecRat;
    private System.Windows.Forms.NumericUpDown numericDecRat;
    private System.Windows.Forms.Label labelDeadDur;
    private System.Windows.Forms.NumericUpDown numericDeadDuration;
    private System.Windows.Forms.Label labelNumLeaf;
    private System.Windows.Forms.Label labelNumLeafInfo;
    private System.Windows.Forms.Label labelLifetime;
    private System.Windows.Forms.Label labelLifetimeInfo;
    private System.Windows.Forms.Label labelTempsum;
    private System.Windows.Forms.Label labelTempsumInfo;
    private System.Windows.Forms.Label labelIPL;
    private System.Windows.Forms.Label labelIPLInfo;
    private System.Windows.Forms.Label labelXPL;
    private System.Windows.Forms.Label labelXPLInfo;
    private System.Windows.Forms.Label labelTempPeak;
    private System.Windows.Forms.Label labelTempPeakInfo;
    private System.Windows.Forms.Button btnTempParamDefault;
    private System.Windows.Forms.Label labelTempPeakTime;
    private System.Windows.Forms.Label labelTempPeakTimeInfo;
    private System.Windows.Forms.Button btnSubtree;
    private System.Windows.Forms.Label labelFocusThrs;
    private System.Windows.Forms.NumericUpDown numericFocused;
  }
}

