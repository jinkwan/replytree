﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class EdgePairComparer : IEqualityComparer<EdgePair>
  {
    bool IEqualityComparer<EdgePair>.Equals(EdgePair x, EdgePair y) {
      if (x.s == y.s && x.t == y.t) return true;
      else return false;
    }

    int IEqualityComparer<EdgePair>.GetHashCode(EdgePair obj) {
      return obj.s;
    }
  }
}
