﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  class ReplyTimeComparer : IComparer<ReplyNode>
  {
    int IComparer<ReplyNode>.Compare(ReplyNode x, ReplyNode y) {
      if (x.timeInfo.timeStamp > y.timeInfo.timeStamp) return 1;
      else if (x.timeInfo.timeStamp == y.timeInfo.timeStamp) return 0;
      else return -1;      
    }
  }
}
  