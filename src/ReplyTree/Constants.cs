﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ReplyTree
{
  static class Constants
  {    
    public const int MAX_MEMBER = 10000;

    public const int COMPO_DISCARD_THRESHOLD = 1;

    public const int WRITER_THRESHOLD = 4;

    public const int CHILD_THRESHOLD = 1;
    public const int ARTICLE_WEIGHT = 10;
    public const int REPLY_WEIGHT = 1;
    public const int PARTICIPATE_WEIGHT = 3;

    public const int FOCUS_CONSTANT = 5;    

    public const int WRITER_DEPTH = 0;
    public const int USER_NET_DEPTH_THRESHOLD = 0;
    public const int ARTICLE_DEPTH_THRESHOLD = -1;

    public const bool CONTAIN_WRITER = true;

    public const int TEMP_INIT = 1000;    

    public const int    TEMP_DURATION = 7200;
    public const int    TEMP_DEAD_INTERVAL = 720;
    public const double TEMP_DEAD_THRESHOLD = 20;
  }
}
