﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using yWorks.Canvas.Drawing;
using yWorks.yFiles.UI.Model;
using yWorks.yFiles.UI.Drawing;
using yWorks.yFiles.Algorithms;

using yWorks.yFiles.Layout;
using yWorks.yFiles.Layout.Circular;
using yWorks.yFiles.Layout.Tree;
using yWorks.yFiles.Layout.Hierarchic;
using yWorks.yFiles.Layout.Organic;
using yWorks.yFiles.Layout.Orthogonal;
using yWorks.yFiles.UI.Input;

namespace ReplyTree
{
  public partial class Form1 : Form
  {   
    /// <summary>
    /// 전체 게시물 목록
    /// </summary>
    List<Article> articleList = new List<Article>();

    /// <summary>
    /// 유저 정보 파일을 기준으로 생성한 유저 정보 목록
    /// </summary>
    Dictionary<string, UserInfo> userList = new Dictionary<string, UserInfo>();

    /// <summary>
    /// 아티클 정보 파일을 기준으로 생성한 유저 정보 목록
    /// </summary>
    //Dictionary<string, UserInfo> actualUserList = new Dictionary<string, UserInfo>();

    /// <summary>
    /// 유저간 대댓글 연결성
    /// </summary>
    //int[,] connectivity = new int[Constants.MAX_MEMBER,Constants.MAX_MEMBER];
    Dictionary<EdgePair, int> conn = new Dictionary<EdgePair, int>(new EdgePairComparer());
    

    Color EDGE_BIDIRECTION = Color.DarkRed;
    Color EDGE_NORMAL = Color.DeepSkyBlue;

    #region Standard Functions(Form, Initiation, Graph Setting, Graph Drawing)
    public Form1() {
      InitializeComponent();
      Initialize();
      GraphSetting();
    }

    private void Initialize() {
      int i, j;
      
      loadArticleBtn.Enabled = false;
      articleListCb.Enabled = false;
      userNetworkBtn.Enabled = false;
      btnNP.Enabled = false;
      btnNV.Enabled = false;
      btnPV.Enabled = false;
      btnDiffNP.Enabled = false;
      btnPortionNP.Enabled = false;

      articleList.Clear();
      userList.Clear();   

      /*
      for (i = 0; i < Constants.MAX_MEMBER; i++) {
        for(j=0;j<Constants.MAX_MEMBER;j++) {
          connectivity[i, j] = 0;
        }
      }
      */
      conn.Clear();
      articleListCb.Items.Clear();
    }

    //화면 view를 움직일 수 있어야
    //노드는 선택해서 움직일 수 있어야
    public void GraphSetting() {
      //GraphViewerInputMode gvim = new GraphViewerInputMode();
      //graphControl1.InputMode = gvim;
      GraphEditorInputMode geim = new GraphEditorInputMode();
      graphControl1.InputMode = geim;
    }

    private void DrawGraph(CanonicMultiStageLayouter l) {
      graphControl1.MorphLayout(l, TimeSpan.FromSeconds(2), null);
    }
    private void radioCircular_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(new CircularLayouter());
    }
    private void radioHierachical_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(new HierarchicLayouter());
    }
    private void radioOrthogonal_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(new OrthogonalLayouter());
    }
    private void radioTree_CheckedChanged(object sender, EventArgs e) {
      //TODO : tree 확인 루틴이 추가되어야 함
      DrawGraph(new TreeLayouter());
    }
    private void radioOrganic_CheckedChanged(object sender, EventArgs e) {
      DrawGraph(new OrganicLayouter());
    }
    #endregion

    #region File I/O

    #region Load Buttons
    private void loadUserBtn_Click(object sender, EventArgs e) {
      LoadUsers();
    }
    private void loadArticleBtn_Click(object sender, EventArgs e) {
      LoadArticles();
      SetActualUserInfo();
      CalcConnectivity();
    }
    #endregion

    #region Loading Functions
    private void LoadUsers() {
      string line;
      char sp;
      FileInfo f;
      StreamReader sr;      

      OpenFileDialog openDlg = new OpenFileDialog();
      openDlg.Filter = "user info file|*.txt";
      openDlg.Multiselect = true;
      if (openDlg.ShowDialog() == DialogResult.OK) {
        Initialize();
        foreach (string filename in openDlg.FileNames) {
          f = new FileInfo(filename);

          sp = '\t';
          sr = f.OpenText();

          while ((line = sr.ReadLine()) != null) {
            string[] spstring = line.Split(sp);
            if (spstring[0][0] == '%') continue;
            else {
              userList[spstring[0]] = new UserInfo(spstring[0], spstring[2], 
                int.Parse(spstring[3]), int.Parse(spstring[4]));              
            }
          }
          sr.Close();
        }
        loadArticleBtn.Enabled = true;
      }
    }
    private void LoadArticles() {
      FileInfo f;
      StreamReader sr;
      string articleLine, replyLine;

      OpenFileDialog openDlg = new OpenFileDialog();
      openDlg.Filter = "article file|*.txt";
      openDlg.Multiselect = true;
      if (openDlg.ShowDialog() == DialogResult.OK) {
        //Initialize();
        foreach (string filename in openDlg.FileNames) {
          f = new FileInfo(filename);
          sr = f.OpenText();
          while ((articleLine = sr.ReadLine()) != null) {
            replyLine = sr.ReadLine();
            AddNewArticle(articleLine, replyLine);
          }
          sr.Close();
        }

        articleListCb.Enabled = true;
        userNetworkBtn.Enabled = true;
        btnNP.Enabled = true;
        btnNV.Enabled = true;
        btnPV.Enabled = true;
        btnDiffNP.Enabled = true;
      }
      logText.AppendText("\nArticle Load Completed");
    }
    private void AddNewArticle(string articleLine, string replyLine) {
      int i;
      char sp = ' ';
      string itemName;
      string[] aInfo = articleLine.Split(sp); //새 아티클 생성
      string[] rInfo = replyLine.Split(sp);

      //글쓴이 depth를 -1로 둠. 
      UserNode parentNode = new UserNode("0", null, Constants.WRITER_DEPTH-1);
      UserNode currNode = new UserNode(rInfo[0], parentNode, parentNode.depth+1);
      Article na = new Article(int.Parse(aInfo[1]), int.Parse(aInfo[2]), currNode);

      for (i = 1; i < rInfo.Length; i++) {
        switch (rInfo[i]) {
          case "(":
            ///현재노드 -> 부모노드
            parentNode = currNode;
            break;
          case ")":
            ///현재노드의 부모노드 -> 부모노드            
            parentNode = currNode.parent.parent;
            currNode = currNode.parent;
            break;
          case "\0":
            //what to do..
            break;
          case "":
            break;
          default:
            ///새노드 => 현재노드
            ///부모노드의 자식 리스트에 추가                  
            currNode = new UserNode(rInfo[i], parentNode, parentNode.depth + 1);
            parentNode.AddChild(currNode);
            if (na.participants.ContainsKey(rInfo[i])) {
              na.participants[rInfo[i]]++;
            }
            else {
              na.participants[rInfo[i]] = 1;
            }
            break;
        }
      }
      na.CalcReplyCountInfo();
      na.CalcTreeInfo();

      //일정 depth 이하로 내려가지 않을 경우 삭제된 글로 판단, 목록에 집어넣지 않음
      if (na.depth > Constants.ARTICLE_DEPTH_THRESHOLD && na.numNodes > 0) {
        articleList.Add(na);
        itemName = na.articleNo.ToString() + " (depth:" + na.depth.ToString()
          + ", numNodes:" + na.numNodes.ToString() + ")";
        articleListCb.Items.Add(itemName);
      }
    }
    public void SetActualUserInfo() {
      foreach(Article article in articleList) {
        userList[article.writer.id].article_actual[article.articleNo] = article.numNodes;
        foreach(string user in article.participants.Keys) {
          userList[user].reply_actual[article.articleNo] = article.participants[user];
        }
      }
    }
    #endregion

    #region Save Buttons
    //참여자수-댓글노드수 그래프
    private void exportBtn_Click(object sender, EventArgs e) {
      CSVFileout("np", 0);
    }
    //댓글수-조회수
    private void btnNV_Click(object sender, EventArgs e) {
      CSVFileout("nv", 1);
    }
    //참여자수-조회수
    private void btnPV_Click(object sender, EventArgs e) {
      CSVFileout("pv", 2);
    }
    //|노드수-참여자수|
    private void btnDiffNP_Click(object sender, EventArgs e) {
      CSVFileout("diffnp", 3);
    }
    //작성자댓글-전체댓글
    private void btnWriterNodes_Click(object sender, EventArgs e) {
      CSVFileout("wn", 4);
    }
    //유저별 작성 글 수 - 획득한 댓글 수
    private void btnTotalWN_Click(object sender, EventArgs e) {
      CSVFileout("twn", 5);
    }

    //유저별 총 작성글, 총 댓글, 댓글단 게시물 수 출력
    private void btnUserInfo_Click(object sender, EventArgs e) {
      CSVFileout("awp", 6);
    }

    public void CSVFileout(string filename, int type) {
      int i;
      string line;
      Article a;
      StreamWriter sw;
      FolderBrowserDialog fd = new FolderBrowserDialog();
      fd.SelectedPath = ".\\";

      if (fd.ShowDialog() == DialogResult.OK) {
        sw = new StreamWriter(fd.SelectedPath + "\\" + filename + ".csv", false, Encoding.UTF8);
        switch (type) {
          case 0:
            line = "articleNo,#replies,#participants";
            sw.WriteLine(line);
            for (i = 0; i < articleList.Count; i++) {
              a = articleList[i];
              line = a.articleNo.ToString() + ",";
              line += a.numNodes.ToString() + ",";
              line += a.participants.Count.ToString();
              sw.WriteLine(line);
            }
            break;
          case 1:
            line = "articleNo,#replies,#views";
            sw.WriteLine(line);
            for (i = 0; i < articleList.Count; i++) {
              a = articleList[i];
              line = a.articleNo.ToString() + ",";
              line += a.numNodes.ToString() + ",";
              line += a.view.ToString();
              sw.WriteLine(line);
            }
            break;

          case 2:
            line = "articleNo,#participants,#views";
            sw.WriteLine(line);
            for (i = 0; i < articleList.Count; i++) {
              a = articleList[i];
              line = a.articleNo.ToString() + ",";
              line += a.participants.Count.ToString() + ",";
              line += a.view.ToString();
              sw.WriteLine(line);
            }
            break;
          case 3:
            int numnode, part;
            line = "articleNo,#replies, #participants, diff(np)";
            sw.WriteLine(line);
            for (i = 0; i < articleList.Count; i++) {
              a = articleList[i];
              numnode = a.numNodes;
              part = a.participants.Count();
              if (a.numNodes > numericNodeThresholdDiff.Value &&
                (numnode - part) > numericDiffThreshold.Value) {
                line = a.articleNo.ToString() + ",";
                line += numnode.ToString() + ",";
                line += part.ToString() + ",";
                line += (numnode - part).ToString();
                sw.WriteLine(line);
              }
            }
            break;
          case 4:
            //todo : 출력하는 내용이 이해가 안 됨. 뭐 하는 것인지 확인해볼 필요
            line = "articleNo,#writerNodes,#nodes";
            sw.WriteLine(line);
            for (i = 0; i < articleList.Count; i++) {
              a = articleList[i];
              line = a.articleNo.ToString() + ",";
              if (a.participants.ContainsKey(a.writer.id)) {
                line += a.participants[a.writer.id].ToString() + ",";
              }
              else {
                line += "0,";
              }
              line += a.numNodes.ToString();
              sw.WriteLine(line);
            }
            break;
          case 5:
            line = "userID,#article,#nodes";
            sw.WriteLine(line);
            foreach(UserInfo user in userList.Values) {
              if(user.article_actual.Count > Constants.WRITER_THRESHOLD) {
                line = user.id + ",";
                line += user.article_actual.Count().ToString() + ",";
                line += user.AcquiredReplies().ToString();                
                sw.WriteLine(line);
              }
            }
            break;
          case 6:
            int actScore = 0;
            //line = "userID,nickname,#write,#reply,#participate";
            line = "userID,#write,#reply,#participate,activity";
            sw.WriteLine(line);
            foreach(UserInfo user in userList.Values) {
              actScore = user.article_actual.Count() * Constants.ARTICLE_WEIGHT;
              actScore += user.TotalReply() * Constants.REPLY_WEIGHT;
              actScore += user.reply_actual.Count() * Constants.PARTICIPATE_WEIGHT;

              //삭제됨, null 제거용
              if (user.reply_actual.Count() == 0) actScore = 0;
              line = user.id + ",";
              //line += user.nickname + ",";
              line += user.article_actual.Count() + ",";
              line += user.TotalReply().ToString() + ",";
              line += user.reply_actual.Count() + ",";
              line += actScore.ToString();              
              sw.WriteLine(line);
            }            
            break;
          default:
            break;
        }
        sw.Close();
      }
    }
    #endregion
    #endregion


    #region Ariticle Level Analysis

    #region Article List Select, Article Info Fileout
    private void articleListCb_SelectedIndexChanged(object sender, EventArgs e) {
      BuildArticleTree(articleListCb.SelectedIndex);
      DrawGraph(new CircularLayouter());
      radioCircular.Checked = true;
      FillArticleInfo();
    }
    private void FillArticleInfo() {
      Article a = articleList[articleListCb.SelectedIndex];
      string line = "";
      //writer information
      line = a.writer.id.ToString();
      line += "(" + userList[a.writer.id].nickname + ", ";
      line += (userList[a.writer.id].article_actual.Count() / 100.0).ToString() + "%)";
      labelWriterInfo.Text = line;

      //number of participants
      labelNumReplyInfo.Text = a.participants.Count.ToString();
      //average depth of all nodes
      labelAvgDepthInfo.Text = a.avgDepth.ToString();
      //depth of tree
      labelDepthInfo.Text = a.depth.ToString();
      //#depth one replies
      labelDepthOneInfo.Text = a.replyOnceCount.ToString();
      //max degree in tree
      labelMDegreeInfo.Text = a.maxDegree.ToString();
      //top replier info
      line = a.mostReplier;
      line += "(" + userList[a.mostReplier].nickname + ", ";
      line += a.participants[a.mostReplier].ToString() + ")";
      labelTopReplierInfo.Text = line;
      //user list of presumably related with the writer
    }
    #endregion

    #region Build Article Tree
    //TODO : 글쓴이는 그래프에 안 넣는게 좋을 것 같음
    private void BuildArticleTree(int idx) {
      int i;
      UserNode p, c;
      Queue<INode> nodeQ = new Queue<INode>();
      Queue<UserNode> userQ = new Queue<UserNode>();
      List<INode> nodeList = new List<INode>();
      List<IEdge> edgeList = new List<IEdge>();
      INode pn, cn;

      graphControl1.Graph.Clear();

      Article a = articleList[idx];
      p = a.writer;
      for (i = 0; i < p.children.Count; i++) {
        c = p.children[i];
        if (c.children.Count != 0) {
          cn = graphControl1.Graph.CreateNode();
          graphControl1.Graph.AddLabel(cn, c.id.ToString());
          userQ.Enqueue(c);
          nodeQ.Enqueue(cn);
        }
      }

      while (userQ.Count > 0) {
        p = userQ.Dequeue();
        pn = nodeQ.Dequeue();

        for (i = 0; i < p.children.Count; i++) {
          c = p.children[i];
          cn = graphControl1.Graph.CreateNode();

          graphControl1.Graph.AddLabel(cn, c.id.ToString());
          graphControl1.Graph.CreateEdge(pn, cn);

          userQ.Enqueue(c);
          nodeQ.Enqueue(cn);
        }
      }
    }
    #endregion

    #endregion


    #region Board Level Analysis

    #region UserNet Button
    private void userNetworkBtn_Click(object sender, EventArgs ea) {
      BuildUserNetwork();
      DrawGraph(new CircularLayouter());
      radioCircular.Checked = true;
    }
    #endregion

    #region User Connectivity Calc, User Network Build
    private void CalcConnectivity() {
      int i, j;
      UserNode p, c;
      Queue<UserNode> userQ = new Queue<UserNode>();
      EdgePair e;

      graphControl1.Graph.Clear();
      for (i = 0; i < articleList.Count; i++) {
        Article a = articleList[i];
        p = a.writer; //root
        userQ.Enqueue(p);

        while (userQ.Count > 0) {
          p = userQ.Dequeue();
          for (j = 0; j < p.children.Count; j++) {
            c = p.children[j];
            userQ.Enqueue(c);
            
            //깊이 얼마 이상부터 의미가 있다고 판단
            if (c.depth >= Constants.USER_NET_DEPTH_THRESHOLD) {
              e = new EdgePair(int.Parse(p.id), int.Parse(c.id));
              if(conn.ContainsKey(e)) {
                conn[e] += 1;
              }
              else {
                conn[e] = 1;
              }              
            }
          }
        }
      }
    }

    //일단 연결 정보에 따라서 모든 노드, 에지 생성
    //제공 알고리즘으로 component 확인해서 
    private void BuildUserNetwork() {
      int i, j, edgeThreshold;
      INode s,t;
      IEdge e;
      Dictionary<int, INode> nodeList = new Dictionary<int, INode>();
      Pen pen;
      AbstractEdgeStyle a = new PolylineEdgeStyle();

      graphControl1.Graph.Clear();
      edgeThreshold = (int)numericEdgeThreshold.Value;
      

      ///dictionary를 사용하면 양방향 처리는 어떻게 할 수 있는지?
      ///에지 목록이 있으면? 이전 에지를 bidirectional로 변경하면 되는지?
      ///각각 하는 것이 제일 편할 것 같기는 함..
      foreach(EdgePair ep in conn.Keys) {
        
      }




      for (i = 0; i < Constants.MAX_MEMBER; i++) {
        for (j = i; j < Constants.MAX_MEMBER; j++) {
          int st = connectivity[i, j];
          int ts = connectivity[j, i];

          // || : 하나라도 되는 경우, && : bidirectional 경우
          if(st > edgeThreshold || ts > edgeThreshold) {

            //node setting
            if (nodeList.ContainsKey(i)) s = nodeList[i];
            else {
              s = graphControl1.Graph.CreateNode();
              graphControl1.Graph.AddLabel(s, i.ToString());
              nodeList[i] = s;
            }
            if (nodeList.ContainsKey(j)) t = nodeList[j];
            else {
              t = graphControl1.Graph.CreateNode();
              graphControl1.Graph.AddLabel(t, j.ToString());
              nodeList[j] = t;
            }

            //edge setting and drawing
            e = st > ts ? graphControl1.Graph.CreateEdge(s, t) : graphControl1.Graph.CreateEdge(t, s);
            pen = new Pen(Brushes.Black, 1);
            pen.Width = st > ts ? ts * (float)0.1 : st * (float)0.1;
            if (st > edgeThreshold && ts > edgeThreshold) {
              pen.Color = EDGE_BIDIRECTION;
              a = new PolylineEdgeStyle(pen);
            }
            else {
              pen.Color = EDGE_NORMAL;
              a = new PolylineEdgeStyle(pen);
              a.TargetArrow = DefaultArrow.Triangle;
            }
            graphControl1.Graph.SetStyle(e, a);
          }
        }
      }

      ///전체 연결된 그래프에서 상위 몇 개의 컴포넌트만 살리고 나머지 죽임
      ///TODO : 컴포넌트 크기 순으로 내림차순 정렬
      ///지금은 맨 처음 것만 남기고 다 죽이므로 괜찮은데,
      ///만약 컴포넌트들이 여러 개의 큰 덩어리로 나뉘어질 경우는 정렬이 필요함      
      YGraphAdapter ga = new YGraphAdapter(graphControl1.Graph);
      if (!GraphConnectivity.IsConnected(ga.YGraph)) {
        INodeMap components = ga.YGraph.CreateNodeMap();
        NodeList[] nl = GraphConnectivity.ConnectedComponents(ga.YGraph);

        for (i = Constants.COMPO_DISCARD_THRESHOLD; i < nl.Count(); i++) {
          INodeCursor nc = nl[i].Nodes();
          while (nc.Ok) {
            graphControl1.Graph.RemoveNode(ga.GetOriginalNode(nc.Node));
            ga.YGraph.Hide(nc.Node);
            nc.Next();
          }
        }
      }
    }
    #endregion

    #endregion

    private void btnMatView_Click(object sender, EventArgs ea) {
      int i, d1,d2,d3,leaf, w, e;

      //for traversal
      UserNode p, c;
      Queue<UserNode> userQ = new Queue<UserNode>();

      //for fileout
      string line;
      StreamWriter sw;
      FolderBrowserDialog fd = new FolderBrowserDialog();
      fd.SelectedPath = ".\\";

      Dictionary<EdgePair, int> edgelist = new Dictionary<EdgePair, int>(new EdgePairComparer());
      EdgePair edge;
      if (fd.ShowDialog() == DialogResult.OK) {
        sw = new StreamWriter(fd.SelectedPath + "\\viewMat_" + 
          Constants.WRITER_DEPTH.ToString() + ".csv", false, Encoding.UTF8);
        line = "article,#reply,#d1,#d2,#d3,depth,#internal,#part,W,#e";
        sw.WriteLine(line);
        foreach (Article a in articleList) {
          
          //정보 생성 과정
          //1.초기화
          d1 = d2 = d3 = leaf = w = e = 0;
          edgelist.Clear();
          //2.tree traversal - d1,2,3,leaf,tmpConn 계산
          userQ.Enqueue(a.writer);
          while (userQ.Count > 0) {
            p = userQ.Dequeue();
            //d1,2,3
            if (p.depth == 1) d1++;
            else if (p.depth == 2) d2++;
            else if (p.depth == 3) d3++;
            //leaf
            if (p.children.Count == 0 && p.depth > Constants.WRITER_DEPTH) leaf++;
            //tmpConn
            for (i = 0; i < p.children.Count; i++) {
              c = p.children[i];
              userQ.Enqueue(c);
              edge = new EdgePair(int.Parse(p.id), int.Parse(c.id));
              
              if(edgelist.ContainsKey(edge)) {
                edgelist[edge] += 1;
              }
              else {
                edgelist[edge] = 1;
              }              
            }
          }

          foreach(EdgePair ep in edgelist.Keys) {
            e += 1;
            w += connectivity[ep.s, ep.t] - edgelist[ep];
          }

          line = a.articleNo.ToString() + ",";
          line += a.numNodes.ToString() + ",";
          line += d1.ToString() + "," + d2.ToString() + "," + d3.ToString() + ",";
          line += a.depth.ToString() + ",";
          line += (a.numNodes - leaf).ToString() + ",";
          line += a.participants.Count.ToString() + ",";          
          line += w.ToString() + ",";
          line += e.ToString();
          sw.WriteLine(line);
        }
        sw.Close();
      }
    }
  }
}
